package com.bdc.treasury.service;

import com.bdc.treasury.core.Utils;
import com.bdc.treasury.entity.CheckerStatus;
import com.bdc.treasury.entity.CrossAccount;
import com.bdc.treasury.entity.TreasuryCtrl;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.global.DataManager;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service(TresuryControlService.NAME)
public class TresuryControlServiceBean implements TresuryControlService {

    @Inject
    private Persistence persistence;

    @Inject
    private DataManager dataManager;
    @Inject
    Utils utils;

    @Override
    public List<TreasuryCtrl> getTreasuryCtrls(LocalDate startDate) throws ParseException {
        List<CrossAccount> accounts = getCorrespondentAcount();

        return accounts.stream()
                .map(crossAccount -> utils.mapToTreasuryCtrl(crossAccount, startDate))
                .collect(Collectors.toList());
    }
    @Override
    public List<CrossAccount> getCorrespondentAcount() throws ParseException {
        return dataManager.load(CrossAccount.class)
                .query("select e from treasury_CrossAccount e  where e.status=:status")
                .parameter("status", CheckerStatus.VALIDATED.name())
                .view("crossAccount-view")
                .list();
    }
}