package com.bdc.treasury.service;

import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import com.haulmont.cuba.core.Transaction;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Service(SCHEDULAR_TRACKING_Service.NAME)
public class SCHEDULAR_TRACKING_ServiceBean implements SCHEDULAR_TRACKING_Service {

    @Inject
    private Persistence persistence;

    @Override
    public List get_SCHEDULAR_TRACKING(String serviceType) {
        Transaction tx = persistence.createTransaction();
        List transactionList = new ArrayList<>();
        try {
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            query.append("select * from SCHEDULAR_TRACKING_STATUS e where e.SERVICE_TYPE = ?1 order by e.ID DESC");
            transactionList = em.createNativeQuery(query.toString())
                    .setParameter(1, serviceType)
                    .setMaxResults(1)
                    .getResultList();
        } finally {
            tx.end();
        }
        tx.end();
        return transactionList;
    }
}