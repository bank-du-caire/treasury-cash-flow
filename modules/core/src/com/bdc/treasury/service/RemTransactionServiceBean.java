package com.bdc.treasury.service;

import com.bdc.treasury.entity.CheckerStatus;
import com.bdc.treasury.entity.CrossAccount;
import com.bdc.treasury.entity.Transaction;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service(RemTransactionService.NAME)
public class RemTransactionServiceBean implements RemTransactionService {

    @Inject
    private Persistence persistence;

    @Override
    public List getRemitanceTransaction(CrossAccount crossAccount, LocalDate startDate, LocalDate endDate) {
        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        List transactionList = new ArrayList<>();
        try {

            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            query.append("select e.VALUE_DATE , SUM(CASE WHEN e.AMOUNT > 0 THEN e.AMOUNT ELSE 0 END) POSITIVE_BALANCE , SUM(CASE WHEN e.AMOUNT < 0 THEN e.AMOUNT ELSE 0 END) NEGATIVE_BALANCE  from REMMITANCE_TRANSACTIONS e where e.CURRENCY like ?1 and e.VALUE_DATE BETWEEN ?2 AND ?3 GROUP BY e.VALUE_DATE");
            transactionList = em.createNativeQuery(query.toString())
                    .setParameter(1, crossAccount.getCurrency().getCode())
                    .setParameter(2, startDate.atTime(0, 0))
                    .setParameter(3, endDate.atTime(23, 59))
                    .getResultList();
        } finally {
            tx.end();

        }


        tx.end();

        return transactionList;
    }

    @Override
    public List getRemitanceTransactionWithinDuration(CrossAccount crossAccount, LocalDate startDate, LocalDate endDate) {
        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        String shortName = (crossAccount.getShortName() != null ? crossAccount.getShortName() : " ");
        List transactionList = new ArrayList<>();
        try {
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            query.append("select e.AMOUNT , e.VALUE_DATE , e.CURRENCY from REMMITANCE_TRANSACTIONS e where e.CURRENCY like ?1 and trunc(e.VALUE_DATE) between ?2 and ?3 and e.STATUS = ?4");
            transactionList = em.createNativeQuery(query.toString())
                    .setParameter(1, crossAccount.getCurrency().getCode())
                    .setParameter(2, startDate)
                    .setParameter(3, endDate)
                    .setParameter(4,"VALIDATED")
                    .getResultList();


        } finally {
            tx.end();

        }


        tx.end();

        return transactionList;
    }

    @Override
    public List getRemitanceTransaction(CrossAccount crossAccount, LocalDate startDate) {
        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        String shortName = (crossAccount.getShortName() != null ? crossAccount.getShortName() : " ");
        List transactionList = new ArrayList<>();
        try {
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            query.append("select e.AMOUNT , e.VALUE_DATE , e.CURRENCY from REMMITANCE_TRANSACTIONS e where e.CURRENCY like ?1 and trunc(e.VALUE_DATE) = ?2  and e.STATUS = ?3");
            transactionList = em.createNativeQuery(query.toString())
                    .setParameter(1, crossAccount.getCurrency().getCode())
                    .setParameter(2, startDate)
                    .setParameter(3,"VALIDATED")
                    .getResultList();


        } finally {
            tx.end();

        }


        tx.end();

        return transactionList;
    }

    @Override
    public List<Timestamp> getLastValueDateRemitance(CrossAccount crossAccount, LocalDate startDate) {
        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        String shortName = (crossAccount.getShortName() != null ? crossAccount.getShortName() : " ");
        List<Timestamp> transactionList = new ArrayList<>();
        try {
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            query.append("select  e.VALUE_DATE  from REMMITANCE_TRANSACTIONS e where e.CURRENCY like ?1 and e.VALUE_DATE < ?2");
            transactionList = em.createNativeQuery(query.toString())
                    .setParameter(1, crossAccount.getCurrency().getCode())
                    .setParameter(2, startDate)
                    .setMaxResults(1)
                    .getResultList();


        } finally {
            tx.end();

        }


        tx.end();

        return transactionList;
    }
}