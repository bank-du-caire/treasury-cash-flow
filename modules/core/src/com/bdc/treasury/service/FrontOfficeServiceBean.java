package com.bdc.treasury.service;

import com.bdc.treasury.entity.CheckerStatus;
import com.bdc.treasury.entity.CrossAccount;
import com.bdc.treasury.entity.Transaction;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service(FrontOfficeService.NAME)
public class FrontOfficeServiceBean implements FrontOfficeService {

    @Inject
    private Persistence persistence;

    @Override
    public List<Transaction> loadAggregateTransactionList(CrossAccount account, LocalDate startDate, LocalDate endDate) {
        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        List<Transaction> transactionList = new ArrayList<>();
        try {
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            query.append("select e.VALUE_DATE, sum(e.CREDIT), SUM(e.DEPT) from TREASURY_TRANSACTION e \n" +
                    "where e.ACCOUNT_ID ='27456751377d59170031e701f84398e9' and \n" +
                    "e.VALUE_DATE BETWEEN :startDate AND :endDate and\n" +
                    " e.STATUS = 'VALIDATED' GROUP BY e.VALUE_DATE");
            transactionList = em.createNativeQuery(query.toString())
                    .setParameter("startDate", startDate)
                    .setParameter("endDate", endDate)
                    .setParameter("state", CheckerStatus.VALIDATED.name())
                    .getResultList();


        } finally {
            tx.end();

        }


        tx.end();
        return transactionList;
    }
}