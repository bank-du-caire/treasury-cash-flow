package com.bdc.treasury.service;

import com.bdc.treasury.entity.CrossAccount;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import com.haulmont.cuba.core.Transaction;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service(DealsService.NAME)
public class DealsServiceBean implements DealsService {

    @Inject
    private Persistence persistence;

    @Override
    public List getDeals(CrossAccount crossAccount, LocalDate startDate, LocalDate endDate) {
        Transaction tx = persistence.createTransaction();
        List transactionList = new ArrayList<>();
        try {
            Integer CPTY_ID = (crossAccount.getCptyId() != null ? crossAccount.getCptyId() : 0);
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            query.append("select e.VALUE_DATE , SUM(CASE WHEN e.AMOUNT > 0 THEN e.AMOUNT ELSE 0 END) POSITIVE_BALANCE , SUM(CASE WHEN e.AMOUNT < 0 THEN e.AMOUNT ELSE 0 END) NEGATIVE_BALANCE from ROOT.DEALS e  where e.CURRENCY_CODE like ?1 and e.VALUE_DATE BETWEEN ?2 AND ?3  GROUP BY e.VALUE_DATE");
            transactionList = em.createNativeQuery(query.toString())
                    .setParameter(1, crossAccount.getCurrency().getCode())
                    .setParameter(2, startDate)
                    .setParameter(3, endDate)
                    .getResultList();

        } finally {
            tx.end();

        }


        tx.end();

        return transactionList;

    }

    @Override
    public List getDeals(CrossAccount crossAccount, LocalDate startDate) {
        Transaction tx = persistence.createTransaction();
        List transactionList = new ArrayList<>();
        try {
            Integer CPTY_ID = (crossAccount.getCptyId() != null ? crossAccount.getCptyId() : 0);
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            //(CASE WHEN d.TRANSACTION_TYPE = ?10 THEN d.AMOUNT ELSE 0 END) CREDIT\n" +
            //                    "    ,(CASE WHEN d.TRANSACTION_TYPE = ?11 THEN d.AMOUNT ELSE 0 END) DEPT\n"

            query.append("select e.AMOUNT , e.CURRENCY_CODE , e.VALUE_DATE ,e.DESCRIPTION , to_char(e.DEAL_ID),e.CURRENCY_CODE,e.RATE,(CASE WHEN e.TRANSACTION_TYPE = ?3 THEN e.AMOUNT ELSE 0 END) CREDIT,(CASE WHEN e.TRANSACTION_TYPE = ?4 THEN e.AMOUNT ELSE 0 END) DEPT from DEALS e where e.CURRENCY_CODE like ?1 and trunc(e.VALUE_DATE) = ?2 ");
            transactionList = em.createNativeQuery(query.toString())
                    .setParameter(1, crossAccount.getCurrency().getCode())
                    .setParameter(2, startDate)
                    .setParameter(3,"Credit")
                    .setParameter(4,"Debit")
                    .getResultList();

        } finally {
            tx.end();

        }


        tx.end();

        return transactionList;

    }

    @Override
    public List<Timestamp> getLastValueDateOfDeals(CrossAccount crossAccount, LocalDate startDate) {
        Transaction tx = persistence.createTransaction();
        List<Timestamp> transactionList = new ArrayList<>();
        try {
            Integer CPTY_ID = (crossAccount.getCptyId() != null ? crossAccount.getCptyId() : 0);
            EntityManager em = persistence.getEntityManager();
            Query f = em.createNativeQuery();
            StringBuilder query = new StringBuilder();
            query.append("select e.VALUE_DATE from DEALS e where e.CURRENCY_CODE like ?1 and e.VALUE_DATE < ?2  order by e.VALUE_DATE DESC");
            transactionList = em.createNativeQuery(query.toString())
                    .setParameter(1, crossAccount.getCurrency().getCode())
                    .setParameter(2, startDate)
                    .setMaxResults(30)
                    .getResultList();

        } finally {
            tx.end();

        }


        tx.end();

        return transactionList;

    }
}