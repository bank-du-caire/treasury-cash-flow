package com.bdc.treasury.service;


import com.bdc.treasury.entity.TransactionsTransfered;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Service(TransactionsTransferedService.NAME)
public class TransactionsTransferedServiceBean implements TransactionsTransferedService {

    @Inject
    private Persistence persistence;

    @Override
    public void updateTransaction(TransactionsTransfered transaction) {
        com.haulmont.cuba.core.Transaction tx = persistence.createTransaction();
        try {
            EntityManager em = persistence.getEntityManager();
            em.merge(transaction);
        }catch (Exception e){
            e.getStackTrace();
        }
        tx.end();
    }
}