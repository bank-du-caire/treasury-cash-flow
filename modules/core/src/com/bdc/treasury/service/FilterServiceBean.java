package com.bdc.treasury.service;

import com.bdc.treasury.entity.CrossAccount;
import com.bdc.treasury.entity.Transaction;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service(FilterService.NAME)
public class FilterServiceBean implements FilterService {

    @Override
    public List<Transaction> filterSearch(List<Transaction> transactionList, CrossAccount accountName) {

        // acc1 - acc2
        // acc1
        return transactionList.stream()
                .filter(transaction -> {
                    if (accountName != null)
                        return !transaction.getAccount().getName().equals(accountName);
                    return false;
                }).collect(Collectors.toList());
    }
}