package com.bdc.treasury.service;

import com.bdc.treasury.entity.CrossAccount;
import com.bdc.treasury.entity.Transaction;

import java.util.List;

public interface FilterService {
    String NAME = "treasury_FilterService";
    public List<Transaction> filterSearch(List<Transaction> transactionList, CrossAccount accountName);
}