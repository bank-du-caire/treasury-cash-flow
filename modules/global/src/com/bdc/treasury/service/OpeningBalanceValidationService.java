package com.bdc.treasury.service;

import com.bdc.treasury.entity.Balance;
import com.bdc.treasury.entity.CrossAccount;


import java.time.LocalDate;


public interface OpeningBalanceValidationService {
    String NAME = "treasury_OpeningBalanceValidationService";
    public boolean validateOpenBalanceExist(CrossAccount crossAccount , LocalDate creationDate);
    public void updateBalance(Balance balance);
}