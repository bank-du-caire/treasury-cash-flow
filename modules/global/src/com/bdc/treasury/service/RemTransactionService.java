package com.bdc.treasury.service;

import com.bdc.treasury.entity.CrossAccount;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

public interface RemTransactionService {
    String NAME = "treasury_RemTransactionService";

    public List getRemitanceTransaction(CrossAccount crossAccount, LocalDate startDate, LocalDate endDate);
    public List getRemitanceTransaction(CrossAccount crossAccount, LocalDate startDate);
    public List getRemitanceTransactionWithinDuration(CrossAccount crossAccount, LocalDate startDate,LocalDate endDate);
    public List<Timestamp> getLastValueDateRemitance(CrossAccount crossAccount, LocalDate startDate);

}