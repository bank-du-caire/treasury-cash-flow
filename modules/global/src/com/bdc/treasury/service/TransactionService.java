package com.bdc.treasury.service;

import com.bdc.treasury.entity.CrossAccount;
import com.bdc.treasury.entity.Transaction;
import com.bdc.treasury.entity.TreasuryCtrl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface TransactionService {
    String NAME = "treasury_TransactionService";

    public Boolean transactionIsExpired(Transaction transaction);

    public void updateTransaction(Transaction transaction);

    public void deleteTransaction(Transaction transaction);

    public List<Transaction>getTransactionCreationDate(Date date);

    public List getRemmitanceList();

    public List getRemmitanceListValidated();

    public void updateRemittanceStatus();

    public void deleteRemittanceStatus();

    public List getTransactionList(CrossAccount crossAccount, LocalDate startDate, LocalDate endDate, boolean accountIsMain);
    public TreasuryCtrl mapToTreasuryCtrl(CrossAccount crossAccount, LocalDate date);
}