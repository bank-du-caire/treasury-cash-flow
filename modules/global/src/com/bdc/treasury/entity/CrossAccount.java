package com.bdc.treasury.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Table(name = "TREASURY_CROSS_ACCOUNT")
@Entity(name = "treasury_CrossAccount")
@NamePattern("%s|name")
public class CrossAccount extends StandardEntity {
    private static final long serialVersionUID = 8740360586624140247L;

    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @JoinColumn(name = "CURRENCY_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Currency currency;

    @Column(name = "SHORT_NAME")
    private String shortName;

    @Column(name = "CPTY_ID")
    private Integer cptyId;

    @Column(name = "MINMUM_BALANCE")
    private BigDecimal minimumBalance;

    @Column(name = "IS_MAIN_ACCOUNT")
    private Boolean isMainAccount;

    @OneToMany(mappedBy = "account")
    private java.util.List<Transaction> transactions;

    @OneToMany(mappedBy = "crossAccount")
    private java.util.List<Balance> balances;

    @Column(name = "NAME")
    private String name;

    @Column(name = "NUMBER_")
    private Integer number;

    @Column(name = "BIC")
    private String bic;

    @Column(name = "CORP_GL")
    private String corporateGl;

    @Column(name = "RET_GL")
    private String retailGl;


    @Temporal(TemporalType.DATE)
    @Column(name = "CREATION_DATE")
    private Date creationDate;

    @JoinTable(name = "TR_CROS_ACC_HDAY_CROS_ACC_LINK",
            joinColumns = @JoinColumn(name = "CROSS_ACCOUNT_ID"),
            inverseJoinColumns = @JoinColumn(name = "CROSS_ACCOUNT_HOLIDAYS_ID"))
    @ManyToMany
    private java.util.List<CrossAccountHolidays> crossAccountHolidayses;

    @JoinTable(name = "CLOSED_ACCT_CROSS_ACCT_LINK",
            joinColumns = @JoinColumn(name = "CROSS_ACCOUNT_ID"),
            inverseJoinColumns = @JoinColumn(name = "CLOSED_CROSS_ACCOUNT_ID"))
    @ManyToMany
    private List<ClosedCrossAccount> closedCrossAccounts;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @NotNull
    @Column(name = "STATUS", nullable = false)
    private String status;

    public Boolean getIsMainAccount() {
        return isMainAccount;
    }

    public void setIsMainAccount(Boolean isMainAccount) {
        this.isMainAccount = isMainAccount;
    }

    public Integer getCptyId() {
        return cptyId;
    }

    public void setCptyId(Integer cptyId) {
        this.cptyId = cptyId;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public BigDecimal getMinimumBalance() {
        return minimumBalance;
    }

    public void setMinimumBalance(BigDecimal minimumBalance) {
        this.minimumBalance = minimumBalance;
    }

    public List<ClosedCrossAccount> getClosedCrossAccounts() {
        return closedCrossAccounts;
    }

    public void setClosedCrossAccounts(List<ClosedCrossAccount> closedCrossAccounts) {
        this.closedCrossAccounts = closedCrossAccounts;
    }

    public void setTransactions(java.util.List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public java.util.List<Transaction> getTransactions() {
        return transactions;
    }

    public void setBalances(java.util.List<Balance> balances) {
        this.balances = balances;
    }

    public java.util.List<Balance> getBalances() {
        return balances;
    }

    public void setCrossAccountHolidayses(java.util.List<CrossAccountHolidays> crossAccountHolidayses) {
        this.crossAccountHolidayses = crossAccountHolidayses;
    }

    public java.util.List<CrossAccountHolidays> getCrossAccountHolidayses() {
        return crossAccountHolidayses;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) throws ParseException {
        this.creationDate = creationDate;
    }
    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getNumber() {
        return number;
    }

    public String getRetailGl() {
        return retailGl;
    }

    public void setRetailGl(String retailGl) {
        this.retailGl = retailGl;
    }

    public String getCorporateGl() {
        return corporateGl;
    }

    public void setCorporateGl(String corporateGl) {
        this.corporateGl = corporateGl;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}