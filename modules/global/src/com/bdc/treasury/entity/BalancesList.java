package com.bdc.treasury.entity;

import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.*;
import java.math.BigDecimal;

@Table(name = "TREASURY_BALANCES_LIST")
@Entity(name = "treasury_BalancesList")
public class BalancesList extends StandardEntity {
    private static final long serialVersionUID = -3538065400620947231L;

    @Column(name = "EXLUDED_AMOUNT")
    private BigDecimal exludedAmount;

    @Column(name = "EXLUDED_AMOUNT_DISCRIPTION")
    private String exludedAmountDiscription;

    @Column(name = "UNKOWN_FUNDS")
    private BigDecimal unkownFunds;

    @Column(name = "UNKOWN_FUNDS_DISCRIPTION")
    private String unkownFundsDiscription;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OPENINGBALANCE_ID")
    private Balance openingbalance;

    public String getUnkownFundsDiscription() {
        return unkownFundsDiscription;
    }

    public void setUnkownFundsDiscription(String unkownFundsDiscription) {
        this.unkownFundsDiscription = unkownFundsDiscription;
    }

    public String getExludedAmountDiscription() {
        return exludedAmountDiscription;
    }

    public void setExludedAmountDiscription(String exludedAmountDiscription) {
        this.exludedAmountDiscription = exludedAmountDiscription;
    }

    public Balance getOpeningbalance() {
        return openingbalance;
    }

    public void setOpeningbalance(Balance openingbalance) {
        this.openingbalance = openingbalance;
    }

    public BigDecimal getUnkownFunds() {
        return unkownFunds;
    }

    public void setUnkownFunds(BigDecimal unkownFunds) {
        this.unkownFunds = unkownFunds;
    }

    public BigDecimal getExludedAmount() {
        return exludedAmount;
    }

    public void setExludedAmount(BigDecimal exludedAmount) {
        this.exludedAmount = exludedAmount;
    }

}