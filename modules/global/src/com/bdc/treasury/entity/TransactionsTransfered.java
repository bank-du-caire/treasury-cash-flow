package com.bdc.treasury.entity;

import com.haulmont.chile.core.annotations.MetaProperty;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;
import org.eclipse.persistence.annotations.CascadeOnDelete;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.LocalDate;

@Table(name = "TRANSACTIONS_TRANSFERED")
@Entity(name = "treasury_TransactionsTransfered")
public class TransactionsTransfered extends StandardEntity {
    private static final long serialVersionUID = -1213062477179492848L;

    @Transient
    @MetaProperty
    private Currency fromAccountCurrency;

    @Transient
    @MetaProperty
    private Currency toAccountCurrency;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FROM_CROSS_ACCOUNT_ID")
    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    private CrossAccount fromCrossAccount;

    @Column(name = "RATE")
    @Positive(message = "Rate must be greater than zero")
    private BigDecimal rate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TO_CROSS_ACCOUNT_ID")
    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    private CrossAccount toCrossAccount;

    @NotNull
    @Column(name = "TRANSFERED_BALANCE", nullable = false)
    private BigDecimal transferedBalance;

    @NotNull
    @Column(name = "VALUE_DATE", nullable = false)
    private LocalDate valueDate;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "STATUS")
    private String status;



    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public Currency getToAccountCurrency() {
        return toAccountCurrency;
    }

    public void setToAccountCurrency(Currency toAccountCurrency) {
        this.toAccountCurrency = toAccountCurrency;
    }

    public CrossAccount getToCrossAccount() {
        return toCrossAccount;
    }

    public void setToCrossAccount(CrossAccount toCrossAccount) {
        this.toCrossAccount = toCrossAccount;
    }

    public CrossAccount getFromCrossAccount() {
        return fromCrossAccount;
    }

    public void setFromCrossAccount(CrossAccount fromCrossAccount) {
        this.fromCrossAccount = fromCrossAccount;
    }

    public Currency getFromAccountCurrency() {
        return fromAccountCurrency;
    }

    public void setFromAccountCurrency(Currency fromAccountCurrency) {
        this.fromAccountCurrency = fromAccountCurrency;
    }

    public LocalDate getValueDate() {
        return valueDate;
    }

    public void setValueDate(LocalDate valueDate) {
        this.valueDate = valueDate;
    }

    public BigDecimal getTransferedBalance() {
        return transferedBalance;
    }

    public void setTransferedBalance(BigDecimal transferedBalance) {
        this.transferedBalance = transferedBalance;
    }

}