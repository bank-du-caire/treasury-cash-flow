package com.bdc.treasury.entity;

import com.haulmont.chile.core.annotations.MetaProperty;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Table(name = "TREASURY_BALANCE")
@Entity(name = "treasury_Balance")
@NamePattern("%s|version")
public class Balance extends StandardEntity {
    private static final long serialVersionUID = 4454651644816375034L;

    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "CROSS_ACCOUNT_ID")
    private CrossAccount crossAccount;

    @Column(name = "STATEMENT_DATE")
    private LocalDate statementDate;

    @Column(name = "CLOSING_DATE")
    private LocalDate closingDate;

    @Column(name = "DEPIT_CREDIT_VALUE")
    private BigDecimal depitCreditValue;

    @Column(name = "CREATION_DATE")
    private LocalDate creationDate;

    @Column(name = "GROUP_ID")
    private UUID groupID;

    @Column(name = "BALANCE")
    @NotNull
    private BigDecimal balance;

    @OneToMany(mappedBy = "openingbalance")
    private List<BalancesList> balancesList;

    @Column(name = "EXLUDED_AMOUNT")
    @NotNull
    private BigDecimal exludedAmount;

    @Column(name = "DEPT")
    @NotNull
    private BigDecimal dept;

    @Column(name = "CREDIT")
    @NotNull
    private BigDecimal credit;

    @Column(name = "UNKOWN_FUNDS")
    @NotNull
    private BigDecimal unkownFunds;

    @Transient
    @MetaProperty
    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    private Currency curruncy;

    @NotNull
    @Column(name = "STATUS", nullable = false)
    private String status;

    @NotNull
    @Column(name = "DEPIT_CREDIT_STATE", nullable = false)
    private String depitCreditState;

    @Column(name = "DESCRIPTION")
    private String description;

    public LocalDate getStatementDate() {
        return statementDate;
    }

    public void setStatementDate(LocalDate statementDate) {
        this.statementDate = statementDate;
    }

    public void setDepitCreditValue(BigDecimal depitCreditValue) {
        this.depitCreditValue = depitCreditValue;
    }

    public BigDecimal getDepitCreditValue() {
        return depitCreditValue;
    }

    public LocalDate getClosingDate() {
        return closingDate;
    }

    public void setClosingDate(LocalDate closingDate) {
        this.closingDate = closingDate;
    }

    public List<BalancesList> getBalancesList() {
        return balancesList;
    }

    public void setBalancesList(List<BalancesList> balancesList) {
        this.balancesList = balancesList;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDepitCreditState(String depitCreditState) {
        this.depitCreditState = depitCreditState;
    }

    public String getDepitCreditState() {
        return depitCreditState;
    }


    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public void setGroupID(UUID groupID) {
        this.groupID = groupID;
    }

    public UUID getGroupID() {
        return groupID;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }


    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Currency getCurruncy() {
        return curruncy;
    }

    public void setCurruncy(Currency curruncy) {
        this.curruncy = curruncy;
    }

    public BigDecimal getUnkownFunds() {
        return unkownFunds;
    }

    public void setUnkownFunds(BigDecimal unkownFunds) {
        this.unkownFunds = unkownFunds;
    }

    public BigDecimal getCredit() {
        return credit;
    }

    public void setCredit(BigDecimal credit) {
        this.credit = credit;
    }

    public BigDecimal getDept() {
        return dept;
    }

    public void setDept(BigDecimal dept) {
        this.dept = dept;
    }

    public BigDecimal getExludedAmount() {
        return exludedAmount;
    }

    public void setExludedAmount(BigDecimal exludedAmount) {
        this.exludedAmount = exludedAmount;
    }

    public CrossAccount getCrossAccount() {
        return crossAccount;
    }

    public void setCrossAccount(CrossAccount crossAccount) {
        this.crossAccount = crossAccount;
    }
}