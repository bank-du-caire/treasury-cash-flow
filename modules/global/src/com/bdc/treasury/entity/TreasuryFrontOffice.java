package com.bdc.treasury.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Table(name = "TREASURY_TREASURY_FRONT_OFFICE")
@Entity(name = "treasury_TreasuryFrontOffice")
@NamePattern("%s|description")
public class TreasuryFrontOffice extends StandardEntity {
    private static final long serialVersionUID = -4478129137323426635L;


    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @JoinColumn(name = "CURRENCY_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Currency currency;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "DEPIT")
    private BigDecimal depit;

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    @Column(name = "RATE")
    private BigDecimal rate;

    @Column(name = "CREDIT")
    private BigDecimal credit;

    @Column(name = "STATE")
    private String state;

    @Column(name = "TOTAL_EXCLUDE")
    private BigDecimal totalExclude;

    @Column(name = "TOTAL_UNKOWN_FUNDS")
    private BigDecimal totalUnkownFunds;

    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ACCOUNT_ID")
    private CrossAccount account;

    @Column(name = "VALUE_DATE")
    private LocalDate valueDate;

    @Column(name = "TRANSACTION_NAME")
    private String departmentName;

    @Column(name = "FINAL_BALANCE")
    private BigDecimal finalBalance;

    @Column(name = "OPENING_BALANCE")
    private BigDecimal openingBalance;

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    @Column(name = "DEAL_ID")
    private String dealId;

    public BigDecimal getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(BigDecimal openingBalance) {
        openingBalance = openingBalance;
    }


    @Column(name = "CLOSING")
    private BigDecimal closing;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getTotalUnkownFunds() {
        return totalUnkownFunds;
    }

    public void setTotalUnkownFunds(BigDecimal totalUnkownFunds) {
        this.totalUnkownFunds = totalUnkownFunds;
    }

    public BigDecimal getTotalExclude() {
        return totalExclude;
    }

    public void setTotalExclude(BigDecimal totalExclude) {
        this.totalExclude = totalExclude;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public BigDecimal getCredit() {
        return credit;
    }

    public void setCredit(BigDecimal credit) {
        this.credit = credit;
    }

    public BigDecimal getDepit() {
        return depit;
    }

    public void setDepit(BigDecimal depit) {
        this.depit = depit;
    }

    public BigDecimal getClosing() {
        return closing;
    }

    public void setClosing(BigDecimal closing) {
        this.closing = closing;
    }


    public BigDecimal getFinalBalance() {
        return finalBalance;
    }

    public void setFinalBalance(BigDecimal finalBalance) {
        this.finalBalance = finalBalance;
    }


    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }


    public LocalDate getValueDate() {
        return valueDate;
    }

    public void setValueDate(LocalDate  valueDate) {
        this.valueDate = valueDate;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public CrossAccount getAccount() {
        return account;
    }

    public void setAccount(CrossAccount account) {
        this.account = account;
    }


}