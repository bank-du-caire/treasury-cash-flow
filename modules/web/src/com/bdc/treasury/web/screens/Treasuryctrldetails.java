package com.bdc.treasury.web.screens;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.service.DealsService;
import com.bdc.treasury.service.RemTransactionService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.gui.components.GroupTable;
import com.haulmont.cuba.gui.components.TextField;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.screen.Screen;
import com.haulmont.cuba.gui.screen.Subscribe;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@UiController("treasury_Treasuryctrldetails")
@UiDescriptor("treasuryCtrlDetails.xml")
public class Treasuryctrldetails extends Screen {
    public LocalDate valueDate;
    public CrossAccount crossAccount;

    @Inject
    private DataManager dataManager;
    @Inject
    private GroupTable<TreasuryFrontOffice> treasuryFrontOfficeTable;
    @Inject
    private CollectionContainer<TreasuryFrontOffice> treasuryFrontOfficesDc;
    @Inject
    private TextField<String> account;
    @Inject
    private TextField<LocalDate> valueDateField;
    @Inject
    private RemTransactionService remTransactionService;
    @Inject
    private DealsService dealsService;
    @Inject
    private TextField<BigDecimal> openingBalanceField;
    @Inject
    private TextField<BigDecimal> totalExcludeAmountField;
    @Inject
    private TextField<BigDecimal> totalUnkownFundsField;
//    @Inject
//    private LookupField<Currency> currency;
//    @Inject
//    private LookupField<Group> deptName;

    public LocalDate getValueDate() {
        return valueDate;
    }

    public void setValueDate(LocalDate valueDate) {
        this.valueDate = valueDate;
    }

    public CrossAccount getCrossAccount() {
        return crossAccount;
    }

    public void setCrossAccount(CrossAccount crossAccount) {
        this.crossAccount = crossAccount;
    }


    List<TreasuryFrontOffice> treasuryFrontOfficeListFinal = new ArrayList<>();
    List<TreasuryFrontOffice> treasuryFrontOfficeListFilterd = new ArrayList<>();

    public List<Transaction> getTransactionWithoutValueDate() {
        List<Transaction> transactionList = new ArrayList<>();
        return transactionList = dataManager.load(Transaction.class)
                .query("select e from treasury_Transaction e where e.account =:account and  e.valueDate is null and e.deleteTs is null ")
                .parameter("account", crossAccount)
                .view("transaction-view")
                .list();
    }

    public List<TreasuryFrontOffice> loadTransaction() {
        account.setValue(crossAccount.getName());
        valueDateField.setValue(valueDate);
        List<TreasuryFrontOffice> treasuryFrontOfficeList = new ArrayList<>();
        if (valueDate != null) {
            List<Transaction> transactionList = dataManager.load(Transaction.class)
                    .query("select e from treasury_Transaction e where e.account =:account and e.valueDate =:valueDate  and (e.status = :stat1 or e.status = :stat2) ORDER BY e.valueDate ASC")
                    .parameter("account", crossAccount)
                    .parameter("valueDate", valueDate)
                    .parameter("stat1", CheckerStatus.VALIDATED.name())
                    .parameter("stat2", Status.TEMPORARY.name())
                    .view("transaction-view")
                    .list();
            treasuryFrontOfficeList =
                    transactionList.stream().map(transaction -> mapToTreasuryFrontOffice(transaction))
                            .collect(Collectors.toList());
        }
        List<TreasuryFrontOffice> treasuryFrontOfficeList1 = (getTransactionWithoutValueDate().stream().map(transaction -> mapToTreasuryFrontOffice(transaction))
                .collect(Collectors.toList()));


        for (TreasuryFrontOffice treasuryFrontOffice : treasuryFrontOfficeList1) {
            treasuryFrontOfficeList.add(treasuryFrontOffice);
        }
 /////////////////////////////////////////////////////////////////////////
        if (crossAccount.getIsMainAccount() != null && crossAccount.getIsMainAccount()) {
            TreasuryFrontOffice remTransactionTotreasuryFrontOffice = dataManager.create(TreasuryFrontOffice.class);
            List treasuryFrontOfficeRemitanceList = remTransactionService.getRemitanceTransaction(crossAccount, valueDate);
            System.out.println("Remittance count = " + treasuryFrontOfficeRemitanceList.size());
            // e.AMOUNT , e.VALUE_DATE , e.CURRENCY
            for (Object o : treasuryFrontOfficeRemitanceList) {
                Object[] raw = ((Object[]) o);
                TreasuryFrontOffice treasuryFrontOffice = dataManager.create(TreasuryFrontOffice.class);
                Timestamp timestamp = (Timestamp) raw[1];
                treasuryFrontOffice.setValueDate(timestamp.toLocalDateTime().toLocalDate());
                treasuryFrontOffice.setCreateTs(timestamp);
                BigDecimal amount = ((BigDecimal) raw[0] != null ? (BigDecimal) raw[0] : BigDecimal.ZERO);
                if ((amount).compareTo(BigDecimal.ZERO) >= 0) {
                    treasuryFrontOffice.setCredit(amount);
                } else {
                    treasuryFrontOffice.setDepit(amount);
                }
                treasuryFrontOffice.setDepartmentName("Remitance Department");
                treasuryFrontOffice.setState("VALIDATED");
                Currency currency = dataManager.create(Currency.class);
                treasuryFrontOffice.setCurrency(currency);
                treasuryFrontOffice.getCurrency().setCode((String) raw[2]);
                treasuryFrontOfficeList.add(treasuryFrontOffice);
            }
        }
        treasuryFrontOfficesDc.getMutableItems().clear();
        treasuryFrontOfficesDc.getMutableItems().addAll(treasuryFrontOfficeList);
        return treasuryFrontOfficeList;
    }

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        loadTransaction();
    }

    public TreasuryFrontOffice mapToTreasuryFrontOffice(Transaction transaction) {
        TreasuryFrontOffice treasuryFrontOffice = dataManager.create(TreasuryFrontOffice.class);
        BigDecimal totalDebit = BigDecimal.ZERO;
        BigDecimal totalCredit = BigDecimal.ZERO;

        treasuryFrontOffice.setValueDate(transaction.getValueDate());
        treasuryFrontOffice.setCredit((transaction.getCredit() != null ? transaction.getCredit() : BigDecimal.ZERO).add(totalCredit));
        treasuryFrontOffice.setDepit((transaction.getDept() != null ? transaction.getDept() : BigDecimal.ZERO).add(totalDebit));
        treasuryFrontOffice.setDepartmentName(transaction.getGroupName());
        treasuryFrontOffice.setState(transaction.getStatus());
        treasuryFrontOffice.setCurrency(dataManager.create(Currency.class));
        treasuryFrontOffice.getCurrency().setCode(transaction.getAccount().getCurrency().getCode());
        treasuryFrontOffice.setCreateTs(transaction.getCreateTs());
        List<Balance> balances = loadOpeningBalancesList(transaction.getAccount(), transaction.getValueDate());
        BigDecimal totalExludedAmount = BigDecimal.ZERO;
        BigDecimal totalUnkownFunds = BigDecimal.ZERO;
        BigDecimal openingBalance = BigDecimal.ZERO;
        for (Balance balance : balances) {
            for (BalancesList balancesList : balance.getBalancesList()) {
                totalExludedAmount = totalExludedAmount.add(balancesList.getExludedAmount() != null ? balancesList.getExludedAmount() : BigDecimal.ZERO);
                totalUnkownFunds = totalUnkownFunds.add(balancesList.getUnkownFunds() != null ? balancesList.getUnkownFunds() : BigDecimal.ZERO);
            }
            openingBalance = balance.getBalance();
        }
        openingBalanceField.setValue(openingBalance);
        totalExcludeAmountField.setValue(totalExludedAmount);
        totalUnkownFundsField.setValue(totalUnkownFunds);

        return treasuryFrontOffice;
    }

    public List<Balance> loadOpeningBalancesList(CrossAccount crossAccount, LocalDate valueDate) {
        List<Balance> balanceList = new ArrayList<>();
        if (crossAccount != null && valueDate != null) {
            balanceList = dataManager.load(Balance.class)
                    .query("select e from treasury_Balance e where e.crossAccount =:crossAccount and e.creationDate =:date and e.status = :state")
                    .parameter("crossAccount", crossAccount)
                    .parameter("date", valueDate)
                    .parameter("state", CheckerStatus.VALIDATED.name())
                    .view("balance-view")
                    .list();
        }
        return balanceList;
    }

}


