package com.bdc.treasury.web.screens.balance;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.service.UserService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.actions.list.CreateAction;
import com.haulmont.cuba.gui.actions.list.EditAction;
import com.haulmont.cuba.gui.actions.list.RefreshAction;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.gui.screen.LookupComponent;
import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import javax.inject.Named;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@UiController("treasury_Balance.browse")
@UiDescriptor("balance-browse.xml")
@LookupComponent("balancesTable")
@LoadDataBeforeShow
public class BalanceBrowse extends StandardLookup<Balance> {

//    private final static String CHECKER_RULE = "checker";
//    private final static String MAKER_RULE = "maker";

    private final static String CHECKER_RULE = "Reconciliation-Role-Checker";
    private final static String MAKER_RULE = "Reconciliation-Role-Maker";
    //    private final static String CONTROL_RULE = "control";
    private final static String CONTROL_maker_RULE = "control-maker";
    private final static String CONTROL_checker_RULE = "control-checker";
    @Inject
    private DataManager dataManager;

    @Named("balancesTable.create")
    private CreateAction<Balance> balancesTableCreate;

    @Named("balancesTable.edit")
    private EditAction<Balance> balancesTableEdit;

    @Inject
    private CollectionContainer<Balance> balancesDc;

    @Inject
    private CollectionContainer<Balance> balancesDc1;

    @Inject
    private UserSession userSession;

    @Inject
    private UserService userService;
    @Inject
    private LookupField<CrossAccount> crossAccount;
    @Inject
    private LookupField<CrossAccount> ValidatedCrossAccount;
    @Inject
    private Notifications notifications;
    @Inject
    private GroupTable<Balance> balancesTable;
    @Inject
    private GroupTable<Balance> balances1Table;
    @Inject
    private DateField<LocalDate> creationDatePending;
    @Inject
    private DateField<LocalDate> creationDateValidated;
    @Inject
    private CollectionLoader<Balance> balancesDl;
    @Inject
    private CollectionLoader<Balance> balancesDl1;
    LoadContext loadContext1;

    @Named("balancesTable.refresh")
    private RefreshAction balancesTableRefresh;
    @Named("balances1Table.refresh")
    private RefreshAction balances1TableRefresh;
    @Inject
    private DateField<LocalDate> StatementDatePending;
    @Inject
    private DateField<LocalDate> StatementDateValidated;

    public List<Balance> pendingFilteredPendingList = new ArrayList<>();
    public List<Balance> validatedFilteredValidatedList = new ArrayList<>();

    @Subscribe
    public void onInit(InitEvent event) {
        balancesDl.setMaxResults(10);
        balancesDl1.setMaxResults(10);
        refreshDataContainers();
        pendingFilteredPendingList = loadPendingBalancesFiltered();
        validatedFilteredValidatedList = loadValidatedBalancesFiltered();
    }

    @Install(to = "balancesDl", target = Target.DATA_LOADER)
    private List<Balance> balancesDlLoadDelegate(LoadContext<Balance> loadContext) {
        loadContext1 = loadContext;
        balancesDl.setMaxResults(1);
        balancesDl1.setMaxResults(1);
        return pendingFilteredPendingList = loadPendingBalances(loadContext1);
    }

    @Install(to = "balancesDl1", target = Target.DATA_LOADER)
    private List<Balance> balancesDl1LoadDelegate(LoadContext<Balance> loadContext) {
        loadContext1 = loadContext;
        balancesDl.setMaxResults(1);
        balancesDl1.setMaxResults(1);
        return validatedFilteredValidatedList = loadValidatedBalances(loadContext1);
    }


    public void refreshDataContainers() {
        balancesTableEdit.setAfterCloseHandler(transaction -> {
            balancesTableRefresh.execute();
            balances1TableRefresh.execute();
        });
        balancesTableCreate.setAfterCloseHandler(transaction -> {
            balancesTableRefresh.execute();
            balances1TableRefresh.execute();
        });
    }

    public List<Balance> loadValidatedBalances(LoadContext loadContext) {
        List<String> rolesNames = userService.getUserRoles();
        balancesDl.setMaxResults(10);
        balancesDl1.setMaxResults(10);
        if (rolesNames.contains(CONTROL_maker_RULE) || rolesNames.contains(CONTROL_checker_RULE)) {
            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select t from treasury_Balance t where t.status =:stat1")
                    .setParameter("stat1", CheckerStatus.VALIDATED.name());
            loadContext.setView("balance-view");
            return dataManager.loadList(loadContext);
        }
        LoadContext.Query query = loadContext.getQuery();
        query.setQueryString("select t from treasury_Balance t where t.status =:stat1 and t.groupID =:groupId")
                .setParameter("stat1", CheckerStatus.VALIDATED.name())
                .setParameter("groupId", userSession.getUser().getGroup().getId());
        loadContext.setView("balance-view");
        return dataManager.loadList(loadContext);
    }

    public List<Balance> loadValidatedBalancesFiltered() {
        List<String> rolesNames = userService.getUserRoles();
        if (rolesNames.contains(CONTROL_maker_RULE) || rolesNames.contains(CONTROL_checker_RULE)) {
            return dataManager.load(Balance.class)
                    .query("select t from treasury_Balance t where t.status =:stat1")
                    .parameter("stat1", CheckerStatus.VALIDATED.name())
                    .view("balance-view")
                    .list();
        }
        return dataManager.load(Balance.class)
                .query("select t from treasury_Balance t where t.status =:stat1 and t.groupID =:groupId")
                .parameter("stat1", CheckerStatus.VALIDATED.name())
                .parameter("groupId", userSession.getUser().getGroup().getId())
                .view("balance-view")
                .list();
    }

    public List<Balance> loadPendingBalances(LoadContext loadContext) {
        List<String> rolesNames = userService.getUserRoles();
        Group group = userSession.getUser().getGroup();
        balancesDl.setMaxResults(10);
        balancesDl1.setMaxResults(10);
        if (rolesNames.contains(CHECKER_RULE)) {

            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select t from treasury_Balance t where t.status =:stat1 and t.groupID =:groupId")
                    .setParameter("stat1", Status.SUBMITTED.name())
                    .setParameter("groupId", group.getId());
            loadContext.setView("balance-view");
            return dataManager.loadList(loadContext);


        } else if (rolesNames.contains(MAKER_RULE)) {
            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select t from treasury_Balance t where (t.status <> :stat2) and t.groupID =:groupId")
                    .setParameter("stat2", CheckerStatus.VALIDATED.name())
                    .setParameter("groupId", group.getId());
            loadContext.setView("balance-view");
            return dataManager.loadList(loadContext);

        }
        return new ArrayList<>();
    }

    public List<Balance> loadPendingBalancesFiltered() {
        List<String> rolesNames = userService.getUserRoles();
        Group group = userSession.getUser().getGroup();

        if (rolesNames.contains(CHECKER_RULE)) {

            return dataManager.load(Balance.class)
                    .query("select t from treasury_Balance t where t.status =:stat1 and t.groupID =:groupId")
                    .parameter("stat1", Status.SUBMITTED.name())
                    .parameter("groupId", group.getId())
                    .view("balance-view")
                    .list();

        } else if (rolesNames.contains(MAKER_RULE)) {

            return dataManager.load(Balance.class)
                    .query("select t from treasury_Balance t where (t.status <> :stat2) and t.groupID =:groupId")
                    .parameter("stat2", CheckerStatus.VALIDATED.name())
                    .parameter("groupId", group.getId())
                    .view("balance-view")
                    .list();
        }
        return new ArrayList<>();
    }

    @Subscribe("crossAccount")
    public void onCrossAccountValueChange(HasValue.ValueChangeEvent<CrossAccount> event) {
        pendingFilteredPendingList = pendingFilteredPendingList.stream().filter(balance -> {
            if (crossAccount.getValue() != null && crossAccount.getValue().getName() != null)
                return balance.getCrossAccount().getName().equals(crossAccount.getValue().getName());
            return false;
        }).collect(Collectors.toList());
        balancesDc.getMutableItems().clear();
        balancesDc.getMutableItems().addAll(pendingFilteredPendingList);
    }

    @Subscribe("ValidatedCrossAccount")
    public void onValidatedCrossAccountValueChange(HasValue.ValueChangeEvent<CrossAccount> event) {
        validatedFilteredValidatedList = validatedFilteredValidatedList.stream().filter(balance -> {
            if (ValidatedCrossAccount.getValue() != null && ValidatedCrossAccount.getValue().getName() != null)
                return balance.getCrossAccount().getName().equals(ValidatedCrossAccount.getValue().getName());
            return false;
        }).collect(Collectors.toList());
        balancesDc1.getMutableItems().clear();
        balancesDc1.getMutableItems().addAll(validatedFilteredValidatedList);
    }


    @Subscribe("balances1Table.clearFilter")
    public void onBalancesTableClearFilter(Action.ActionPerformedEvent event) {
        ValidatedCrossAccount.setValue(null);
        creationDateValidated.setValue(null);
        StatementDateValidated.setValue(null);
        balances1TableRefresh.execute();
        balancesTableRefresh.execute();
    }

    @Subscribe("balancesTable.clearFilter")
    public void onBalancesTableClearFilter1(Action.ActionPerformedEvent event) {
        crossAccount.setValue(null);
        creationDatePending.setValue(null);
        StatementDatePending.setValue(null);
        balancesTableRefresh.execute();
        balances1TableRefresh.execute();
    }

    @Subscribe("VALIDATEDbtn")
    public void onVALIDATEDbtnClick(Button.ClickEvent event) {
        if (balancesTable.getSelected().size() > 0) {
            Set<Balance> balanceList = balancesTable.getSelected();
            for (Balance balance : balanceList) {
                balance.setStatus(CheckerStatus.VALIDATED.name());
                balancesDc.getItem().setStatus(CheckerStatus.VALIDATED.name());
                dataManager.commit(balance);
            }
            balancesTableRefresh.execute();
            balances1TableRefresh.execute();
        } else {
            message();
        }
    }

    @Subscribe("RETURNEDbtn")
    public void onRETURNEDbtnClick(Button.ClickEvent event) {
        if (balancesTable.getSelected().size() > 0) {
            Set<Balance> balanceList = balancesTable.getSelected();
            for (Balance balance : balanceList) {
                balance.setStatus(CheckerStatus.RETURNED.name());
                balancesDc.getItem(balance).setStatus(CheckerStatus.RETURNED.name());
                dataManager.commit(balance);
            }
            balancesTableRefresh.execute();
            balances1TableRefresh.execute();
        } else {
            message();
        }
    }

    @Subscribe("RETURNEDValidatebtn")
    public void onRETURNEDValidatebtnClick(Button.ClickEvent event) {
        if (balances1Table.getSelected().size() > 0) {
            Set<Balance> balanceList = balances1Table.getSelected();
            for (Balance balance : balanceList) {
                balance.setStatus(CheckerStatus.RETURNED.name());
                balancesDc1.getItem(balance).setStatus(CheckerStatus.RETURNED.name());
                dataManager.commit(balance);
            }
            balancesTableRefresh.execute();
            balances1TableRefresh.execute();
        } else {
            message();
        }
    }

    @Subscribe("SUBMITTEDbtn")
    public void onSUBMITTEDbtnClick(Button.ClickEvent event) {
        if (balancesTable.getSelected().size() > 0) {
            Set<Balance> balanceList = balancesTable.getSelected();
            for (Balance balance : balanceList) {
                if (!balance.getStatus().equals(Status.SUBMITTED.name())) {
                    balance.setStatus(Status.SUBMITTED.name());
                    balancesTable.setSelected(balance);
                    balancesDc.getItem(balance).setStatus(Status.SUBMITTED.name());
                    dataManager.commit(balance);
                }
            }
            balancesTableRefresh.execute();
            balances1TableRefresh.execute();
        } else {
            message();
        }
    }

    public void message() {
        notifications.create().withCaption("You should select one row at least").show();
    }

    @Subscribe("creationDatePending")
    public void onCreationDatePendingValueChange(HasValue.ValueChangeEvent<LocalDate> event) {
        pendingFilteredPendingList = pendingFilteredPendingList.stream().filter(balance -> {
            if (creationDatePending.getValue() != null)
                return balance.getCreationDate().equals(creationDatePending.getValue());
            return false;
        }).collect(Collectors.toList());
        balancesDc.getMutableItems().clear();
        balancesDc.getMutableItems().addAll(pendingFilteredPendingList);
    }

    @Subscribe("creationDateValidated")
    public void onCreationDateValidatedValueChange(HasValue.ValueChangeEvent<LocalDate> event) {
        validatedFilteredValidatedList = validatedFilteredValidatedList.stream().filter(balance -> {
            if (creationDateValidated.getValue() != null)
                return balance.getCreationDate().equals(creationDateValidated.getValue());
            return false;
        }).collect(Collectors.toList());
        balancesDc1.getMutableItems().clear();
        balancesDc1.getMutableItems().addAll(validatedFilteredValidatedList);
    }

    @Subscribe("StatementDatePending")
    public void onStatementDateTransactionValueChange(HasValue.ValueChangeEvent<LocalDate> event) {
        pendingFilteredPendingList = pendingFilteredPendingList.stream().filter(transaction1 -> {
            if (StatementDatePending.getValue() != null && transaction1.getStatementDate() != null)
                return transaction1.getStatementDate().equals(StatementDatePending.getValue());
            return false;
        }).collect(Collectors.toList());
        balancesDc.getMutableItems().clear();
        balancesDc.getMutableItems().addAll(pendingFilteredPendingList);
    }

    @Subscribe("StatementDateValidated")
    public void onStatementDateValidatedValueChange(HasValue.ValueChangeEvent<LocalDate> event) {
        validatedFilteredValidatedList = validatedFilteredValidatedList.stream().filter(transaction1 -> {
            if (StatementDateValidated.getValue() != null && transaction1.getStatementDate() != null)
                return transaction1.getStatementDate().equals(StatementDateValidated.getValue());
            return false;
        }).collect(Collectors.toList());
        balancesDc1.getMutableItems().clear();
        balancesDc1.getMutableItems().addAll(validatedFilteredValidatedList);
    }


}