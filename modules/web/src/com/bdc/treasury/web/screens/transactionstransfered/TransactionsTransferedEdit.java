package com.bdc.treasury.web.screens.transactionstransfered;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.service.TransactionService;
import com.bdc.treasury.service.TransactionsTransferedService;
import com.bdc.treasury.service.UserService;
import com.haulmont.cuba.core.global.CommitContext;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@UiController("treasury_TransactionsTransfered.edit")
@UiDescriptor("transactions-transfered-edit.xml")
@EditedEntityContainer("transactionsTransferedDc")
@LoadDataBeforeShow
public class TransactionsTransferedEdit extends StandardEditor<TransactionsTransfered> {
    @Inject
    private LookupField<Currency> fromAccountCurrencyField;
    @Inject
    private DataManager dataManager;
    @Inject
    private CollectionContainer<CrossAccount> fromCrossAccountsDc;
    @Inject
    private LookupField<CrossAccount> fromCrossAccountField;
    @Inject
    private Metadata metadata;

    @Inject
    private DateField<LocalDate> valueDateField;
    @Inject
    private TextField<BigDecimal> transferedBalanceField;
    @Inject
    private LookupField<CrossAccount> toCrossAccountField;
    @Inject
    private Notifications notifications;


    @Inject
    private UserSession userSession;
    @Inject
    private UserService userService;
    @Inject
    TransactionsTransferedService transactionsTransferedService;
    @Inject
    private InstanceContainer<TransactionsTransfered> transactionsTransferedDc;

    public List<Transaction> transactionList = new ArrayList<>();
    @Inject
    private CollectionContainer<Transaction> transactionDc;

    @Inject
    TransactionService transactionService;

    public String status = " ";
    public String globalStatus = "";
    @Inject
    private CollectionContainer<CrossAccount> toCrossAccountsDc;

    private final static String CONTROL_maker_RULE = "control-maker";
    private final static String CONTROL_checker_RULE = "control-checker";
    @Inject
    private LookupField<String> makerStatus;

    @Subscribe
    public void onInit(InitEvent event) {
        List<CrossAccount> accountList = new ArrayList<>();
        fromCrossAccountField.setOptionsList(accountList);
        toCrossAccountField.setOptionsList(accountList);

        List<String> controlMakerList = new ArrayList<>();
        List<String> controlCheckerList = new ArrayList<>();

        controlMakerList.add(Status.SAVED.name());
        controlMakerList.add(Status.SUBMITTED.name());
        controlCheckerList.add(CheckerStatus.VALIDATED.name());
        controlCheckerList.add(CheckerStatus.RETURNED.name());

        if (userService.getUserRoles().contains(CONTROL_maker_RULE)) {
            makerStatus.setOptionsList(controlMakerList);
        } else if (userService.getUserRoles().contains(CONTROL_checker_RULE)) {
            fromCrossAccountField.setEditable(false);
            toCrossAccountField.setEditable(false);
            fromAccountCurrencyField.setEditable(false);
            valueDateField.setEditable(false);
            makerStatus.setOptionsList(controlCheckerList);
        }
    }

    @Subscribe
    public void onBeforeCommitChanges(BeforeCommitChangesEvent event) {
        if (fromCrossAccountField.getValue().equals(toCrossAccountField.getValue())) {
            notifications.create().withCaption("From Correspondent Account and To Correspondent Account are identical").show();
            event.preventCommit();
        }
        if (userService.getUserRoles().contains(CONTROL_checker_RULE)
                && makerStatus.getValue().equals(CheckerStatus.RETURNED.name())
                && globalStatus.equals(CheckerStatus.VALIDATED.name())) {
            notifications.create().withCaption("Not Allow to return validated Status").show();
            event.preventCommit();
        }
    }

    @Subscribe
    public void onAfterCommitChanges(AfterCommitChangesEvent event) {
        if (userService.getUserRoles().contains(CONTROL_checker_RULE) && transactionsTransferedDc.getItem().getStatus() != null
                && transactionsTransferedDc.getItem().getStatus().equals(CheckerStatus.VALIDATED.name())
                && !status.equals("edit")) {
            TransactionsTransfered transactionsTransfered = transactionsTransferedDc.getItem();
            Transaction transaction1 = metadata.create(Transaction.class);
            Transaction transaction2 = metadata.create(Transaction.class);
//        if (status.equals("create")) {
            Group group = userSession.getUser().getGroup();
            List<String> rolesNames = userService.getUserRoles();
            transaction1.setAccount(fromCrossAccountField.getValue());
            transaction1.setDept(transferedBalanceField.getValue());
            transaction1.setCredit(BigDecimal.ZERO);
            transaction1.setValueDate(valueDateField.getValue());
            transaction1.setStatus(CheckerStatus.VALIDATED.name());
            transaction1.setGroupName(group.getName());
            transaction1.setTransactionsTransfered(transactionsTransfered);
            dataManager.commit(transaction1);
            //////////////////////
            transaction2.setAccount(toCrossAccountField.getValue());
            transaction2.setDept(BigDecimal.ZERO);
            transaction2.setCredit(transferedBalanceField.getValue());
            transaction2.setValueDate(valueDateField.getValue());
            transaction2.setStatus(CheckerStatus.VALIDATED.name());
            transaction2.setGroupName(group.getName());
            transaction2.setTransactionsTransfered(transactionsTransfered);
            dataManager.commit(transaction2);
        } else if (status.equals("edit")) {
            for (Transaction transaction : transactionList) {
                if (transaction.getDept().compareTo(BigDecimal.ZERO) != 0) {
                    transaction.setDept(transferedBalanceField.getValue());
                } else if (transaction.getCredit().compareTo(BigDecimal.ZERO) != 0) {
                    transaction.setCredit(transferedBalanceField.getValue());
                }
                transactionService.updateTransaction(transaction);
            }
        }


    }

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        // know status of transaction
        if (transactionsTransferedDc.getItem().getStatus() != null) {
            globalStatus = transactionsTransferedDc.getItem().getStatus();
        }
        TransactionsTransfered transactionsTransfered = (transactionsTransferedDc.getItem() != null ? transactionsTransferedDc.getItem() : null);
        if (transactionsTransfered != null) {
            transactionList = dataManager.load(Transaction.class)
                    .query("select e from treasury_Transaction e where e.transactionsTransfered =:transactionsTransfered")
                    .parameter("transactionsTransfered", transactionsTransfered)
                    .view("transaction-view")
                    .list();
        }
        transactionDc.getMutableItems().clear();
        transactionDc.getMutableItems().addAll(transactionList);
        if (transactionsTransferedDc.getItem().getStatus() != null && transactionsTransferedDc.getItem().getStatus().equals(CheckerStatus.VALIDATED.name())) {
            status = "edit";
        }
//        else {
//            status = "edit";
//        }
    }

    @Subscribe("fromAccountCurrencyField")
    public void onFromAccountCurrencyFieldValueChange(HasValue.ValueChangeEvent<Currency> event) {
        if (fromAccountCurrencyField.getValue() != null && fromAccountCurrencyField.getValue().getName() != null) {
            List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
                    .query("select t from treasury_CrossAccount t where t.currency =:currency and t.status=:status")
                    .parameter("status",CheckerStatus.VALIDATED.name())
                    .parameter("currency", fromAccountCurrencyField.getValue())
                    .view("crossAccount-view")
                    .list();
            fromCrossAccountsDc.getMutableItems().clear();
            fromCrossAccountsDc.getMutableItems().addAll(crossAccountList);
            fromCrossAccountField.setOptionsList(crossAccountList);
            toCrossAccountsDc.getMutableItems().clear();
            toCrossAccountsDc.getMutableItems().addAll(crossAccountList);
            toCrossAccountField.setOptionsList(crossAccountList);
        }
    }
}