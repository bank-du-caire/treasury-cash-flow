package com.bdc.treasury.web.screens.transaction;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.service.ClosedAccountService;
import com.bdc.treasury.service.CrossAccountHolidayService;
import com.bdc.treasury.service.UserService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.actions.list.CreateAction;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.DataContext;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@UiController("treasury_Transaction.edit")
@UiDescriptor("transaction-edit.xml")
@EditedEntityContainer("transactionDc")
@LoadDataBeforeShow
public class TransactionEdit extends StandardEditor<Transaction> {

    private final static String CHECKER_RULE = "checker";
    //    private final static String CONTROL_RULE = "control";
    private final static String CONTROL_maker_RULE = "control-maker";
    private final static String CONTROL_checker_RULE = "control-checker";

    @Inject
    private UserSession userSession;

    @Inject
    private LookupField<String> checkerStatusField;

    @Inject
    private LookupField<String> statusField;

    @Inject
    private InstanceContainer<Transaction> transactionDc;

    @Inject
    private Notifications notifications;

    @Inject
    private UserService userService;

    @Inject
    private CrossAccountHolidayService crossAccountHolidayService;
    @Inject
    private DataManager dataManager;
    @Inject
    private CollectionContainer<CrossAccount> accountsDc;
    @Inject
    private LookupField<Currency> currencyField;
    @Inject
    private ClosedAccountService closedAccountService;
    private final static Long TEMP_TIME = 5L; // 5 minutes
    String oldStatus = new String();

    @Inject
    private DateField<LocalDate> valueDateField;
    @Inject
    private LookupField<CrossAccount> accountField;
    @Inject
    private TextField<BigDecimal> creditField;
    @Inject
    private TextField<BigDecimal> deptField;


    @Subscribe
    public void onInit(InitEvent event) {
        List<String> checkerList = new ArrayList<>();
        List<String> makerList = new ArrayList<>();
        List<String> controlMakerList = new ArrayList<>();
        List<String> controlCheckerList = new ArrayList<>();

        checkerList.add(CheckerStatus.RETURNED.name());
        checkerList.add(CheckerStatus.VALIDATED.name());

        makerList.add(Status.SAVED.name());
        makerList.add(Status.SUBMITTED.name());

        controlMakerList.add(Status.SAVED.name());
        controlMakerList.add(Status.SUBMITTED.name());

        controlCheckerList.add(Status.TEMPORARY.name());
        controlCheckerList.add(CheckerStatus.RETURNED.name());
        controlCheckerList.add(CheckerStatus.VALIDATED.name());

        if (userService.getUserRoles().contains(CONTROL_maker_RULE)) {
//            checkerStatusField.setOptionsList(controlList);
            statusField.setOptionsList(controlMakerList);


        } else if (userService.getUserRoles().contains(CONTROL_checker_RULE)) {
            checkerStatusField.setOptionsList(controlCheckerList);
        } else if (userService.getUserRoles().contains(CHECKER_RULE)) {
            checkerStatusField.setOptionsList(checkerList);

        } else {
            statusField.setOptionsList(makerList);
        }

        List<CrossAccount> accountList = new ArrayList<>();
        accountField.setOptionsList(accountList);
    }


    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        if (transactionDc.getItem() != null && transactionDc.getItem().getStatus() != null)
            oldStatus = transactionDc.getItem().getStatus();
    }


    @Subscribe(target = Target.DATA_CONTEXT)
    public void onPreCommit(DataContext.PreCommitEvent event) {

        Group group = userSession.getUser().getGroup();
        List<String> rolesNames = userService.getUserRoles();
        Transaction transaction = transactionDc.getItem();
        transaction.setGroupName(group.getName());
        if (!userService.getUserRoles().contains(CONTROL_maker_RULE) && !userService.getUserRoles().contains(CONTROL_checker_RULE)) {
            transaction.setGroupID(group.getId());
            if (valueDateField.getValue() == null) {
                event.preventCommit();
                notifications.create().withCaption("Value date must be filled").show();
            }
        }
        LocalDate valueDate = transaction.getValueDate();
        LocalDate now = LocalDate.now();
        if (valueDate != null && valueDate.isBefore(now) && !rolesNames.contains(CHECKER_RULE)
                && !rolesNames.contains(CONTROL_maker_RULE) && !rolesNames.contains(CONTROL_checker_RULE)) {
            event.preventCommit();
            notifications.create().withCaption("Value date must be after current date").show();
        }
        if (transaction.getAccount() != null && transaction.getValueDate() != null) {
            if (valueDate != null && !valueDate.isBefore(now) && crossAccountHolidayService.checkHolidaysOfCrossAccount(transaction.getAccount(), transaction.getValueDate())) {
                event.preventCommit();
                notifications.create().withCaption("The value date of this correspondent account ( " + transaction.getAccount().getName() + " ) matches with a holiday day.").show();
            }
        }
        if (transaction.getAccount() != null && transaction.getValueDate() != null) {
            if (valueDate != null && !valueDate.isBefore(now) && closedAccountService.checkOfClosedCrossAccount(transaction.getAccount(), transaction.getValueDate(), CloseAccountState.CLOSED.name())) {
                event.preventCommit();
                notifications.create().withCaption("This account ( " + transaction.getAccount().getName() + " ) is closed with value date = " + transaction.getValueDate()).show();
            }
        }
        if (oldStatus.equals(Status.TEMPORARY.name())) {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date currentDate = new Date();
            Date updateTs = transaction.getUpdateTs();
            LocalDateTime localDateTimeUpdateTs = new Timestamp(updateTs.getTime()).toLocalDateTime();
            LocalDateTime currentLocalDateTime = LocalDateTime.now();
            //check same date
            boolean condition1 = fmt.format(transaction.getUpdateTs()).equals(fmt.format(currentDate));
            boolean condition2 = currentLocalDateTime.getHour() == localDateTimeUpdateTs.getHour();
            boolean condition3 = currentLocalDateTime.getMinute() - localDateTimeUpdateTs.getMinute() <= TEMP_TIME;
            if ((condition1 && condition2 && condition3) == false) {
                event.preventCommit();
                notifications.create().withCaption("Time expired to edit this transaction").show();
            }
        }
        transactionDc.getItem().setCreationDate(now);
        if (userService.getUserRoles().contains(CONTROL_maker_RULE)) {
            if (creditField.getValue() != null && deptField.getValue() != null) {
                event.preventCommit();
                notifications.create().withCaption("Not Allowed to add credit and debit Together").show();
            }
        }
        if (rolesNames.contains(CONTROL_checker_RULE) && transactionDc.getItem().getGroupName().equals("Treasury Control") && transactionDc.getItem().getStatus().equals(CheckerStatus.RETURNED.name())) {
            event.preventCommit();
            notifications.create().withCaption("Not allowed to return this Transaction").show();
        }
    }


    @Subscribe("currencyField")
    public void onCurrencyFieldValueChange(HasValue.ValueChangeEvent<Currency> event) {
        if (currencyField.getValue() != null && currencyField.getValue().getName() != null) {
            List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
                    .query("select t from treasury_CrossAccount t where t.currency =:currency and t.status=:status")
                    .parameter("status",CheckerStatus.VALIDATED.name())
                    .parameter("currency", currencyField.getValue())
                    .view("crossAccount-view")
                    .list();
            accountsDc.getMutableItems().clear();
            accountsDc.getMutableItems().addAll(crossAccountList);
            accountField.setOptionsList(crossAccountList);
        } else {
            List<CrossAccount> crossAccountList = new ArrayList<>();
            accountField.setOptionsList(crossAccountList);
        }
    }
}