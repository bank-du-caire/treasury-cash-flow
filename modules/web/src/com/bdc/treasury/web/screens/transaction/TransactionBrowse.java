package com.bdc.treasury.web.screens.transaction;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.service.FilterService;
import com.bdc.treasury.service.TransactionService;
import com.bdc.treasury.service.TresuryControlService;
import com.bdc.treasury.service.UserService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.actions.list.CreateAction;
import com.haulmont.cuba.gui.actions.list.EditAction;
import com.haulmont.cuba.gui.actions.list.RefreshAction;
import com.haulmont.cuba.gui.actions.list.RemoveAction;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.gui.screen.LookupComponent;
import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.global.UserSession;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@UiController("treasury_Transaction.browse")
@UiDescriptor("transaction-browse.xml")
@LookupComponent("transactionsTable")
@LoadDataBeforeShow
public class TransactionBrowse extends StandardLookup<Transaction> {


    private final static String CHECKER_RULE = "checker";
    private final static String MAKER_RULE = "maker";
//    private final static String CONTROL_RULE = "control";

    private final static String CONTROL_maker_RULE = "control-maker";
    private final static String CONTROL_checker_RULE = "control-checker";
    @Inject
    private DataManager dataManager;

    @Inject
    private UserSession userSession;

    @Inject
    private CollectionContainer<Transaction> transactionsDc;

    @Inject
    private CollectionContainer<Transaction> transactionsDc1;

    @Named("transactionsTable.edit")
    private EditAction<Transaction> transactionsTableEdit;

    @Named("transactionsTable.create")
    private CreateAction<Transaction> transactionsTableCreate;

    @Named("transactionsTable.refresh")
    private RefreshAction transactionsTableRefresh;

    @Named("validatedTransactionsTable.refresh")
    private RefreshAction validatedTransactionsTableRefresh;
    @Inject
    private UserService userService;

    @Inject
    private LookupField<CrossAccount> crossAccount;

    @Inject
    private LookupField<CrossAccount> validatedCrossAccount;

    @Inject
    private TransactionService transactionService;

    public List<Transaction> transactionFilteredPendingList = new ArrayList<>();
    public List<Transaction> transactionFilteredValidatedList = new ArrayList<>();
    @Inject
    private DateField<LocalDate> valueDateTransaction;
    @Inject
    private LookupField<Group> groupTransction;
    @Inject
    private TextField<String> ReferenceNumberTransaction;
    @Inject
    private TextField<String> ReferenceNumberValidated;
    @Inject
    private LookupField<Group> groupValidated;
    @Inject
    private DateField<LocalDate> valueDateValidated;
    @Inject
    private GroupTable<Transaction> transactionsTable;
    @Inject
    private Notifications notifications;
    @Inject
    private GroupTable<Transaction> validatedTransactionsTable;
    @Inject
    private DateField<LocalDate> creationDateTransaction;
    @Inject
    private DateField<LocalDate> creationDateValidated;
    @Inject
    private CollectionLoader<Transaction> transactionsDl;
    @Inject
    private CollectionLoader<Transaction> transactionsDl1;
    @Inject
    private Logger log;
    LoadContext loadContext1;

    @Inject
    private TextField<BigDecimal> totalCredit;
    @Inject
    private TextField<BigDecimal> totalDebit;
    @Inject
    private TextField<BigDecimal> netBalance;


    @Subscribe
    public void onInit(InitEvent event) {
        refreshDataContainers();
        transactionsDl.setMaxResults(10);
        transactionsDl1.setMaxResults(10);
        transactionFilteredPendingList = loadPendingDataFilterd();
        transactionFilteredValidatedList = loadValidatedDataFiltered();

    }

    @Install(to = "transactionsDl1", target = Target.DATA_LOADER)
    private List<Transaction> transactionsDl1LoadDelegate(LoadContext<Transaction> loadContext) {
        loadContext1 = loadContext;
        transactionsDl.setMaxResults(10);
        transactionsDl1.setMaxResults(10);
        return transactionFilteredValidatedList = loadValidatedData(loadContext1);
    }

    @Install(to = "transactionsDl", target = Target.DATA_LOADER)
    private List<Transaction> transactionsDlLoadDelegate(LoadContext<Transaction> loadContext) {
        loadContext1 = loadContext;
        transactionsDl.setMaxResults(10);
        transactionsDl1.setMaxResults(10);
        return transactionFilteredPendingList = loadPendingData(loadContext1);
    }

    public void refreshDataContainers() {
        transactionsTableEdit.setAfterCloseHandler(transaction -> {
//            transactionsDc.getMutableItems().clear();
//            transactionsDc.getMutableItems().addAll(loadPendingData(loadContext1));
            transactionsTableRefresh.execute();

//            transactionsDc1.getMutableItems().clear();
//            transactionsDc1.getMutableItems().addAll(loadValidatedData(loadContext1));
            validatedTransactionsTableRefresh.execute();
        });
        transactionsTableCreate.setAfterCloseHandler(transaction -> {
//            transactionsDc.getMutableItems().clear();
//            transactionsDc.getMutableItems().addAll(loadPendingData(loadContext1));
            transactionsTableRefresh.execute();

//            transactionsDc1.getMutableItems().clear();
//            transactionsDc1.getMutableItems().addAll(loadValidatedData(loadContext1));
            validatedTransactionsTableRefresh.execute();
        });
    }

    public List<Transaction> loadValidatedData(LoadContext loadContext) {
        transactionsDl.setMaxResults(10);
        transactionsDl1.setMaxResults(10);
        LoadContext.Query query = loadContext.getQuery();
        query.setQueryString("select t from treasury_Transaction t where (t.status =:stat1) and t.groupID =:groupId")
                .setParameter("stat1", CheckerStatus.VALIDATED.name())
                .setParameter("groupId", userSession.getUser().getGroup().getId());
        loadContext.setView("transaction-view");
        return dataManager.loadList(loadContext);
    }

    public List<Transaction> loadValidatedDataFiltered() {
        return dataManager.load(Transaction.class)
                .query("select t from treasury_Transaction t where (t.status =:stat1) and t.groupID =:groupId")
                .parameter("stat1", CheckerStatus.VALIDATED.name())
                .parameter("groupId", userSession.getUser().getGroup().getId())
                .view("transaction-view")
                .list();

    }

    public List<Transaction> loadPendingData(LoadContext loadContext) {
        List<String> rolesNames = userService.getUserRoles();
        List<Transaction> transactionList = new ArrayList<>();
        transactionsDl.setMaxResults(10);
        transactionsDl1.setMaxResults(10);
        if (rolesNames.contains(CHECKER_RULE)) {
            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select t from treasury_Transaction t where (t.status =:stat1 or t.status =:stat2 ) and t.groupID =:groupId")
                    .setParameter("stat1", Status.SUBMITTED.name())
                    .setParameter("stat2", Status.TEMPORARY.name())
                    .setParameter("groupId", userSession.getUser().getGroup().getId());
            loadContext.setView("transaction-view");
            transactionList = dataManager.loadList(loadContext);
            transactionList = transactionList.stream()
                    .filter(transaction -> {
                        if (transaction.getStatus().equals(Status.TEMPORARY.name())) {
                            return transactionService.transactionIsExpired(transaction);
                        }
                        return true;
                    }).collect(Collectors.toList());

        } else if (rolesNames.contains(MAKER_RULE)) {
            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select t from treasury_Transaction t where (t.status =:stat1 or t.status <> :stat2) and t.groupID =:groupId")
                    .setParameter("stat1", Status.SUBMITTED.name())
                    .setParameter("stat2", CheckerStatus.VALIDATED.name())
                    .setParameter("groupId", userSession.getUser().getGroup().getId());
            loadContext.setView("transaction-view");
            transactionList = dataManager.loadList(loadContext);
            transactionList = transactionList.stream()
                    .filter(transaction -> {
                        if (transaction.getStatus().equals(Status.TEMPORARY.name())) {
                            return transactionService.transactionIsExpired(transaction);
                        }
                        return true;
                    }).collect(Collectors.toList());
        } else if (rolesNames.contains(CONTROL_maker_RULE)) {
            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select t from treasury_Transaction t where (t.status = :stat1 or t.status = :stat2 or t.status = :stat3) and t.groupName = :groupName ")
                    .setParameter("stat1", Status.SUBMITTED.name())
                    .setParameter("stat2", CheckerStatus.RETURNED.name())
                    .setParameter("stat3", Status.SAVED.name())
                    .setParameter("groupName", "Treasury Control");
            loadContext.setView("transaction-view");
            transactionList = dataManager.loadList(loadContext);
        }
        if (rolesNames.contains(CONTROL_checker_RULE)) {
            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select t from treasury_Transaction t where (t.status = :stat1 ) or t.status = :stat2 or t.status=:stat3")
                    .setParameter("stat1", Status.SUBMITTED.name())
                    .setParameter("stat2", Status.TEMPORARY.name())
                    .setParameter("stat3", CheckerStatus.VALIDATED.name());
            loadContext.setView("transaction-view");
            transactionList = dataManager.loadList(loadContext);
        }
        transactionsDl.setMaxResults(10);
        transactionsDl1.setMaxResults(10);
        return transactionList;
    }

    public List<Transaction> loadPendingDataFilterd() {
        List<String> rolesNames = userService.getUserRoles();
        List<Transaction> transactionList = new ArrayList<>();
        if (rolesNames.contains(CHECKER_RULE)) {
            transactionList = dataManager.load(Transaction.class)
                    .query("select t from treasury_Transaction t where (t.status =:stat1 or t.status =:stat2 ) and t.groupID =:groupId")
                    .parameter("stat1", Status.SUBMITTED.name())
                    .parameter("stat2", Status.TEMPORARY.name())
                    .parameter("groupId", userSession.getUser().getGroup().getId())
                    .view("transaction-view")
                    .list();
            transactionList = transactionList.stream()
                    .filter(transaction -> {
                        if (transaction.getStatus().equals(Status.TEMPORARY.name())) {
                            return transactionService.transactionIsExpired(transaction);
                        }
                        return true;
                    }).collect(Collectors.toList());

        } else if (rolesNames.contains(MAKER_RULE)) {
            transactionList = dataManager.load(Transaction.class)
                    .query("select t from treasury_Transaction t where (t.status =:stat1 or t.status <> :stat2) and t.groupID =:groupId")
                    .parameter("stat1", Status.SUBMITTED.name())
                    .parameter("stat2", CheckerStatus.VALIDATED.name())
                    .parameter("groupId", userSession.getUser().getGroup().getId())
                    .view("transaction-view")
                    .list();
            transactionList = transactionList.stream()
                    .filter(transaction -> {
                        if (transaction.getStatus().equals(Status.TEMPORARY.name())) {
                            return transactionService.transactionIsExpired(transaction);
                        }
                        return true;
                    }).collect(Collectors.toList());
        }
        if (rolesNames.contains(CONTROL_maker_RULE)) {
            transactionList = dataManager.load(Transaction.class)
                    .query("select t from treasury_Transaction t where (t.status = :stat1 or t.status = :stat2 or t.status = :stat3) and t.groupName = :groupName ")
                    .parameter("stat1", Status.SUBMITTED.name())
                    .parameter("stat2", CheckerStatus.RETURNED.name())
                    .parameter("stat3", Status.SAVED.name())
                    .parameter("groupName", "Treasury Control")
                    .view("transaction-view")
                    .list();
        }
        if (rolesNames.contains(CONTROL_checker_RULE)) {
            transactionList = dataManager.load(Transaction.class)
                    .query("select t from treasury_Transaction t where (t.status = :stat1 and t.groupName =:groupName) or t.status = :stat2 or t.status=:stat3")
                    .parameter("stat1", Status.SUBMITTED.name())
                    .parameter("stat2", Status.TEMPORARY.name())
                    .parameter("groupName", "Treasury Control")
                    .parameter("stat3", CheckerStatus.VALIDATED.name())
                    .view("transaction-view")
                    .list();
        }

        return transactionList;
    }

    @Subscribe("crossAccount")
    public void onCrossAccountValueChange(HasValue.ValueChangeEvent<CrossAccount> event) {
//        transactionFilteredPendingList = loadPendingDataFilterd();
        transactionFilteredPendingList = transactionFilteredPendingList.stream().filter(transaction1 -> {
            if (crossAccount.getValue() != null && crossAccount.getValue().getName() != null)
                return transaction1.getAccount().getName().equals(crossAccount.getValue().getName());
            return false;
        }).collect(Collectors.toList());
        transactionsDc.getMutableItems().clear();
        transactionsDc.getMutableItems().addAll(transactionFilteredPendingList);

        // total debit & Total Credit
        updateTotalCredit_Debit_net_Transaction();
    }

    public void updateTotalCredit_Debit_net_Transaction() {
        if (crossAccount.getValue() != null && crossAccount.getValue().getName() != null
                && valueDateTransaction.getValue() != null) {
            TreasuryCtrl treasuryCtrl = dataManager.create(TreasuryCtrl.class);
            treasuryCtrl = transactionService.mapToTreasuryCtrl(crossAccount.getValue(), valueDateTransaction.getValue());
            totalCredit.setValue(treasuryCtrl.getTotalCredit());
            totalDebit.setValue(treasuryCtrl.getTotalDebit());
            netBalance.setValue(treasuryCtrl.getBalance());
        }
    }
    public void updateTotalCredit_Debit_net_Validated() {
        if (validatedCrossAccount.getValue() != null && validatedCrossAccount.getValue().getName() != null
                && valueDateValidated.getValue() != null) {
            TreasuryCtrl treasuryCtrl = dataManager.create(TreasuryCtrl.class);
            treasuryCtrl = transactionService.mapToTreasuryCtrl(validatedCrossAccount.getValue(), valueDateValidated.getValue());
            totalCredit.setValue(treasuryCtrl.getTotalCredit());
            totalDebit.setValue(treasuryCtrl.getTotalDebit());
            netBalance.setValue(treasuryCtrl.getBalance());
        }
    }

    @Subscribe("groupTransction")
    public void onGroupTransctionValueChange(HasValue.ValueChangeEvent<String> event) {
//        transactionFilteredPendingList = loadPendingDataFilterd();
        transactionFilteredPendingList = transactionFilteredPendingList.stream().filter(transaction1 -> {
            if (groupTransction.getValue() != null) {
                if (transaction1.getGroupName() != null)
                    return transaction1.getGroupName().equals(groupTransction.getValue().getName());
            }
            return false;
        }).collect(Collectors.toList());
        transactionsDc.getMutableItems().clear();
        transactionsDc.getMutableItems().addAll(transactionFilteredPendingList);
    }

    @Subscribe("ReferenceNumberTransaction")
    public void onReferenceNumberTransactionEnterPress(TextInputField.EnterPressEvent event) {
//        transactionFilteredPendingList = loadPendingDataFilterd();
        transactionFilteredPendingList = transactionFilteredPendingList.stream().filter(transaction1 -> {
            if (ReferenceNumberTransaction.getValue() != null) {
                if (transaction1.getReferenceNumber() != null) {
                    return transaction1.getReferenceNumber().equals(ReferenceNumberTransaction.getValue());
                }
            }
            return false;
        }).collect(Collectors.toList());
        transactionsDc.getMutableItems().clear();
        transactionsDc.getMutableItems().addAll(transactionFilteredPendingList);
    }


    @Subscribe("valueDateTransaction")
    public void onValueDateTransactionValueChange(HasValue.ValueChangeEvent<Date> event) {
//        transactionFilteredPendingList = loadPendingDataFilterd();
        transactionFilteredPendingList = transactionFilteredPendingList.stream().filter(transaction1 -> {
            if (valueDateTransaction.getValue() != null)
                if (transaction1.getValueDate() != null)
                    return transaction1.getValueDate().equals(valueDateTransaction.getValue());
            return false;
        }).collect(Collectors.toList());
        transactionsDc.getMutableItems().clear();
        transactionsDc.getMutableItems().addAll(transactionFilteredPendingList);

        // total Debit && total credit && net
        updateTotalCredit_Debit_net_Transaction();
    }

    @Subscribe("creationDateTransaction")
    public void onCreationDateTransactionValueChange(HasValue.ValueChangeEvent<LocalDate> event) {
//        transactionFilteredPendingList = loadPendingDataFilterd();
        transactionFilteredPendingList = transactionFilteredPendingList.stream().filter(transaction1 -> {
            if (creationDateTransaction.getValue() != null)
                if (transaction1.getCreationDate() != null)
                    return transaction1.getCreationDate().equals(creationDateTransaction.getValue());
            return false;
        }).collect(Collectors.toList());
        transactionsDc.getMutableItems().clear();
        transactionsDc.getMutableItems().addAll(transactionFilteredPendingList);
    }

    @Subscribe("creationDateValidated")
    public void onCreationDateValidatedValueChange(HasValue.ValueChangeEvent<LocalDate> event) {
//        transactionFilteredValidatedList = loadValidatedDataFiltered();
        transactionFilteredValidatedList = transactionFilteredValidatedList.stream().filter(transaction1 -> {
            if (creationDateValidated.getValue() != null)
                if (transaction1.getCreationDate() != null)
                    return transaction1.getCreationDate().equals(creationDateValidated.getValue());
            return false;
        }).collect(Collectors.toList());
        transactionsDc1.getMutableItems().clear();
        transactionsDc1.getMutableItems().addAll(transactionFilteredValidatedList);
    }


    //filter
    @Subscribe("validatedCrossAccount")
    public void onValidatedCrossAccountValueChange(HasValue.ValueChangeEvent<CrossAccount> event) {
        transactionFilteredValidatedList = transactionFilteredValidatedList.stream().filter(transaction -> {
            if (validatedCrossAccount.getValue() != null && validatedCrossAccount.getValue().getName() != null)
                return transaction.getAccount().getName().equals(validatedCrossAccount.getValue().getName());
            return false;
        }).collect(Collectors.toList());
        transactionsDc1.getMutableItems().clear();
        transactionsDc1.getMutableItems().addAll(transactionFilteredValidatedList);
    }


    @Subscribe("transactionsTable.clearFilter")
    public void onCustomerBasesTableClearFilter(Action.ActionPerformedEvent event) {
        crossAccount.setValue(null);
        valueDateTransaction.setValue(null);
        groupTransction.setValue(null);
        ReferenceNumberTransaction.setValue(null);
        creationDateTransaction.setValue(null);
        transactionsTableRefresh.execute();
    }

    @Subscribe("validatedTransactionsTable.clearFilter")
    public void onValidatedTransactionsTableClearFilter(Action.ActionPerformedEvent event) {
        validatedCrossAccount.setValue(null);
        ReferenceNumberValidated.setValue(null);
        valueDateValidated.setValue(null);
        groupValidated.setValue(null);
        creationDateValidated.setValue(null);
        validatedTransactionsTableRefresh.execute();
    }

    @Subscribe("ReferenceNumberValidated")
    public void onReferenceNumberValidatedEnterPress(TextInputField.EnterPressEvent event) {
//        transactionFilteredValidatedList = loadValidatedDataFiltered();
        transactionFilteredValidatedList = transactionFilteredValidatedList.stream().filter(transaction1 -> {
            if (ReferenceNumberValidated.getValue() != null) {
                if (transaction1.getReferenceNumber() != null) {
                    return transaction1.getReferenceNumber().equals(ReferenceNumberValidated.getValue());
                }
            }
            return false;
        }).collect(Collectors.toList());
        transactionsDc1.getMutableItems().clear();
        transactionsDc1.getMutableItems().addAll(transactionFilteredValidatedList);
    }

    @Subscribe("groupValidated")
    public void onGroupValidatedValueChange(HasValue.ValueChangeEvent<Group> event) {
//        transactionFilteredValidatedList = loadValidatedDataFiltered();
        transactionFilteredValidatedList = transactionFilteredValidatedList.stream().filter(transaction1 -> {
            if (groupValidated.getValue() != null) {
                if (transaction1.getGroupName() != null)
                    return transaction1.getGroupName().equals(groupValidated.getValue().getName());
            }
            return false;
        }).collect(Collectors.toList());
        transactionsDc1.getMutableItems().clear();
        transactionsDc1.getMutableItems().addAll(transactionFilteredValidatedList);
    }

    @Subscribe("valueDateValidated")
    public void onValueDateValidatedValueChange(HasValue.ValueChangeEvent<LocalDate> event) {
//        transactionFilteredPendingList = loadValidatedDataFiltered();
        transactionFilteredValidatedList = transactionFilteredValidatedList.stream().filter(transaction1 -> {
            if (valueDateValidated.getValue() != null)
                return transaction1.getValueDate().equals(valueDateValidated.getValue());
            return false;
        }).collect(Collectors.toList());
        transactionsDc1.getMutableItems().clear();
        transactionsDc1.getMutableItems().addAll(transactionFilteredValidatedList);
    }


    @Subscribe("VALIDATEDbtn")
    public void onVALIDATEDbtnClick(Button.ClickEvent event) {
        if (transactionsTable.getSelected().size() > 0) {
            Set<Transaction> transactions = transactionsTable.getSelected();
            for (Transaction transaction : transactions) {
                transaction.setStatus(CheckerStatus.VALIDATED.name());
                transactionsDc.getItem().setStatus(CheckerStatus.VALIDATED.name());
                dataManager.commit(transaction);
            }
            transactionsTableRefresh.execute();
            validatedTransactionsTableRefresh.execute();
        } else {
            message();
        }
    }

    @Subscribe("RETURNEDbtn")
    public void onRETURNEDbtnClick(Button.ClickEvent event) {
        List<String> rolesNames = userService.getUserRoles();
        if (transactionsTable.getSelected().size() > 0) {
            Set<Transaction> transactions = transactionsTable.getSelected();
            for (Transaction transaction : transactions) {
                if (rolesNames.contains(CONTROL_checker_RULE) && !transaction.getGroupName().equals("Treasury Control")) {
                    notifications.create().withCaption("Not allowed to return this Transaction").show();
                } else {
                    transaction.setStatus(CheckerStatus.RETURNED.name());
                    transactionsDc.getItem(transaction).setStatus(CheckerStatus.RETURNED.name());
                    dataManager.commit(transaction);
                }
            }
            transactionsTableRefresh.execute();
            validatedTransactionsTableRefresh.execute();
        } else {
            message();
        }
    }

    @Subscribe("RETURNEDValidatebtn")
    public void onRETURNEDValidatebtnClick(Button.ClickEvent event) {
        List<String> rolesNames = userService.getUserRoles();
        if (validatedTransactionsTable.getSelected().size() > 0) {
            Set<Transaction> transactions = validatedTransactionsTable.getSelected();
            for (Transaction transaction : transactions) {
                if (rolesNames.contains(CONTROL_checker_RULE) && !transaction.getGroupName().equals("Treasury Control")) {
                    notifications.create().withCaption("Not allowed to return this Transaction").show();
                } else {
                    transaction.setStatus(CheckerStatus.RETURNED.name());
                    transactionsDc1.getItem(transaction).setStatus(CheckerStatus.RETURNED.name());
                    dataManager.commit(transaction);
                }
            }
            transactionsTableRefresh.execute();
            validatedTransactionsTableRefresh.execute();
        } else {
            message();
        }
    }

    @Subscribe("SUBMITTEDbtn")
    public void onSUBMITTEDbtnClick(Button.ClickEvent event) {
        if (transactionsTable.getSelected().size() > 0) {
            Set<Transaction> transactions = transactionsTable.getSelected();
            for (Transaction transaction : transactions) {
                if (!transaction.getStatus().equals(Status.SUBMITTED.name())) {
                    transaction.setStatus(Status.SUBMITTED.name());
                    transactionsTable.setSelected(transaction);
                    transactionsDc.getItem(transaction).setStatus(Status.SUBMITTED.name());
                    dataManager.commit(transaction);
                }
            }
            transactionsTableRefresh.execute();
            validatedTransactionsTableRefresh.execute();
        } else {
            message();
        }
    }

    public void message() {
        notifications.create().withCaption("You should select one row at least").show();
    }

    @Named("transactionsTable.remove")
    private RemoveAction customersTableRemove;


    @Subscribe("transactionsTable.remove")
    public void onTransactionsTransferedsTableRemove(Action.ActionPerformedEvent event) {
        List<String> rolesNames = userService.getUserRoles();
        Transaction transaction = transactionsTable.getSingleSelected();
        if (rolesNames.contains(CONTROL_checker_RULE) && rolesNames.contains(CONTROL_maker_RULE) && transaction.getGroupName().equals("Treasury Control")) {
            customersTableRemove.execute();
        } else if (rolesNames.contains(CONTROL_checker_RULE) && rolesNames.contains(CONTROL_maker_RULE) && !transaction.getGroupName().equals("Treasury Control")) {
            notifications.create().withCaption("Not allowed to delete this transaction").show();
        } else if (transactionsTable.getSingleSelected() != null && !transaction.getStatus().equals(Status.SUBMITTED.name())) {
            customersTableRemove.execute();
        } else if (transactionsTable.getSingleSelected() != null && transaction.getStatus().equals(Status.SUBMITTED.name())) {
            notifications.create().withCaption("Not allowed to remove SUBMITTED transaction").show();

        } else {
            customersTableRemove.execute();
        }


    }
}