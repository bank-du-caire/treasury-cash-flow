package com.bdc.treasury.web.screens.crossaccount;

import com.bdc.treasury.entity.Balance;
import com.bdc.treasury.entity.CheckerStatus;
import com.bdc.treasury.entity.Status;
import com.bdc.treasury.service.UserService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.gui.actions.list.CreateAction;
import com.haulmont.cuba.gui.actions.list.EditAction;
import com.haulmont.cuba.gui.actions.list.RefreshAction;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.data.CollectionDatasource;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.treasury.entity.CrossAccount;
import com.haulmont.cuba.gui.screen.LookupComponent;
import com.haulmont.cuba.gui.xml.layout.ComponentsFactory;
import com.vaadin.ui.Alignment;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@UiController("treasury_CrossAccount.browse")
@UiDescriptor("cross-account-browse.xml")
@LookupComponent("crossAccountsTable")
@LoadDataBeforeShow
public class CrossAccountBrowse extends StandardLookup<CrossAccount> {

    @Inject
    private TextField<String> bic;
    @Inject
    private TextField<String> corporateGl;
    @Inject
    private TextField<String> retailGl;
    @Inject
    private TextField<Integer> number;
    @Inject
    private LookupField<String> currency;
    @Inject
    private TextField<CrossAccount> crossName;

    @Inject
    private CollectionLoader<CrossAccount> crossAccountsDl;

    @Named("crossAccountsTable.refresh")
    private RefreshAction crossAccountsTable;

    @Named("crossAccountsTable.create")
    private CreateAction<CrossAccount> crossAccountsTableCreate;
    @Named("crossAccountsTable.edit")
    private EditAction<CrossAccount> crossAccountsTableEdit;

    private final static String ReconciliationRoleMaker = "Reconciliation-Role-Maker";
    private final static String ReconciliationRoleChecker = "Reconciliation-Role-Checker";
    @Inject
    private UserService userService;
    @Inject
    private DataManager dataManager;

    @Subscribe("crossAccountsTable.clearFilter")
    public void onCustomerBasesTableClearFilter(Action.ActionPerformedEvent event) {
        number.setValue(null);

        crossName.setValue(null);
        currency.setValue(null);
        retailGl.setValue(null);
        corporateGl.setValue(null);
        bic.setValue(null);
    }

    @Subscribe
    public void onInit(InitEvent event) {
        refreshDataContainers();
        crossAccountsDl.setMaxResults(10);
    }

    @Install(to = "crossAccountsDl", target = Target.DATA_LOADER)
    private List<CrossAccount> crossAccountsDlLoadDelegate(LoadContext<CrossAccount> loadContext) {
        if (!userService.getUserRoles().contains(ReconciliationRoleMaker) && !userService.getUserRoles().contains(ReconciliationRoleChecker)) {
            return getValidatedCrossAccount();
        } else if (userService.getUserRoles().contains(ReconciliationRoleMaker)) {
                return getCrossAccountReconilationMaker();
        } else if (userService.getUserRoles().contains(ReconciliationRoleChecker)) {
                return getCrossAccountReconilationChecker();
        }

        return new ArrayList<>();
    }

    public List<CrossAccount> getValidatedCrossAccount() {
        List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
                .query("select t from treasury_CrossAccount t where  t.status=:status")
                .parameter("status", CheckerStatus.VALIDATED.name())
                .view("crossAccount-view")
                .list();
        return crossAccountList;

    }

    public List<CrossAccount> getCrossAccountReconilationMaker() {
        List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
                .query("select t from treasury_CrossAccount t where  t.status<>:status")
                .parameter("status", CheckerStatus.VALIDATED.name())
                .view("crossAccount-view")
                .list();
        return crossAccountList;

    }
    public List<CrossAccount> getCrossAccountReconilationChecker() {
        List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
                .query("select t from treasury_CrossAccount t where  t.status=:status or t.status=:status2")
                .parameter("status", CheckerStatus.VALIDATED.name())
                .parameter("status2", Status.SUBMITTED.name())
                .view("crossAccount-view")
                .list();
        return crossAccountList;

    }
    public void refreshDataContainers() {
        crossAccountsTableEdit.setAfterCloseHandler(transaction -> {
            crossAccountsTable.execute();

        });
        crossAccountsTableCreate.setAfterCloseHandler(transaction -> {
            crossAccountsTable.execute();

        });
    }

}