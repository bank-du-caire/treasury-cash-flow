package com.bdc.treasury.web.screens.currency;

import com.bdc.treasury.entity.CheckerStatus;
import com.bdc.treasury.entity.CrossAccount;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.treasury.entity.Currency;

import javax.inject.Inject;
import java.util.List;

@UiController("treasury_Currency.edit")
@UiDescriptor("currency-edit.xml")
@EditedEntityContainer("currencyDc")
@LoadDataBeforeShow
public class CurrencyEdit extends StandardEditor<Currency> {
    @Inject
    private DataManager dataManager;
    @Inject
    private CollectionContainer<CrossAccount> crossAccountsDc;
    @Inject
    private LookupField<CrossAccount> accountField;

    @Subscribe
    public void onInit(InitEvent event) {
        List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
                .query("select t from treasury_CrossAccount t where t.status=:status")
                .parameter("status", CheckerStatus.VALIDATED.name())
                .view("crossAccount-view")
                .list();
        crossAccountsDc.getMutableItems().clear();
        crossAccountsDc.getMutableItems().addAll(crossAccountList);
        accountField.setOptionsList(crossAccountList);
    }

}