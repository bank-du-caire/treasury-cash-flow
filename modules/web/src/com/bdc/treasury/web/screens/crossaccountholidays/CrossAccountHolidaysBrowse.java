package com.bdc.treasury.web.screens.crossaccountholidays;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.service.UserService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.gui.actions.list.CreateAction;
import com.haulmont.cuba.gui.actions.list.EditAction;
import com.haulmont.cuba.gui.actions.list.RefreshAction;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@UiController("treasury_CrossAccountHolidays.browse")
@UiDescriptor("cross-account-holidays-browse.xml")
@LookupComponent("crossAccountHolidaysesTable")
@LoadDataBeforeShow
public class CrossAccountHolidaysBrowse extends StandardLookup<CrossAccountHolidays> {
    @Inject
    private CollectionLoader<CrossAccountHolidays> crossAccountHolidaysesDl;
    @Named("crossAccountHolidaysesTable.edit")
    private EditAction<Transaction> crossAccountHolidaysesEdit;

    @Named("crossAccountHolidaysesTable.create")
    private CreateAction<Transaction> crossAccountHolidaysesCreate;

    @Named("crossAccountHolidaysesTable.refresh")
    private RefreshAction crossAccountHolidaysesRefresh;
    LoadContext loadContext1;
    @Subscribe
    public void onInit(InitEvent event) {
        crossAccountHolidaysesDl.setMaxResults(10);
        refreshDataContainers();
    }
    @Inject
    private UserService userService;
    private final static String Holiday_maker_RULE = "Holiday-maker";
    private final static String Holiday_checker_RULE = "Holiday-checker";
    @Inject
    private DataManager dataManager;

    @Install(to = "crossAccountHolidaysesDl", target = Target.DATA_LOADER)
    private List<CrossAccountHolidays> crossAccountHolidaysesDlLoadDelegate(LoadContext<CrossAccountHolidays> loadContext) {
        loadContext1 = loadContext;
        return loadPendingData(loadContext1);
    }

    public List<CrossAccountHolidays> loadPendingData(LoadContext loadContext) {
        List<String> rolesNames = userService.getUserRoles();
        List<CrossAccountHolidays> accountHolidaysList = new ArrayList<>();
        if (rolesNames.contains(Holiday_maker_RULE)) {
            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select t from treasury_CrossAccountHolidays t where (t.status = :stat1 or t.status = :stat2 or t.status = :stat3)")
                    .setParameter("stat1", Status.SUBMITTED.name())
                    .setParameter("stat2", CheckerStatus.RETURNED.name())
                    .setParameter("stat3", Status.SAVED.name());
            accountHolidaysList = dataManager.loadList(loadContext);
        }
        if (rolesNames.contains(Holiday_checker_RULE)) {
            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select t from treasury_CrossAccountHolidays t where (t.status = :stat1 ) or t.status=:stat3")
                    .setParameter("stat1", Status.SUBMITTED.name())
                    .setParameter("stat3", CheckerStatus.VALIDATED.name());
            accountHolidaysList = dataManager.loadList(loadContext);
        }
        crossAccountHolidaysesDl.setMaxResults(10);
        return accountHolidaysList;
    }
    public void refreshDataContainers() {
        crossAccountHolidaysesEdit.setAfterCloseHandler(transaction -> {
            crossAccountHolidaysesRefresh.execute();
        });
        crossAccountHolidaysesCreate.setAfterCloseHandler(transaction -> {
            crossAccountHolidaysesRefresh.execute();
        });
    }
}