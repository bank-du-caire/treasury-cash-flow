package com.bdc.treasury.web.screens;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.service.TransactionService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.GroupTable;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.screen.Screen;
import com.haulmont.cuba.gui.screen.Subscribe;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@UiController("treasury_Remittance")
@UiDescriptor("Remittance.xml")
public class Remittance extends Screen {
    @Inject
    private CollectionContainer<Transaction> RemittanceDc;
    @Inject
    TransactionService transactionService;
    @Inject
    private DataManager dataManager;
    @Inject
    private GroupTable<Transaction> remittanceTable;
    @Inject
    private Notifications notifications;
    @Inject
    private CollectionContainer<Transaction> RemittanceDc1;

    @Subscribe
    public void onInit(InitEvent event) {
        loadTransaction();
        loadTransactionValidated();
    }
    public List<Transaction> loadTransaction(){
        List<Transaction> transactionList = new ArrayList<>();

        List remitanceList = transactionService.getRemmitanceList();
        for (Object o : remitanceList) {
            Object[] raw = ((Object[]) o);
            Transaction transaction = dataManager.create(Transaction.class);
            Currency currency = dataManager.create(Currency.class);
            currency.setName((String) raw[1]);
            CrossAccount crossAccount = dataManager.create(CrossAccount.class);
            crossAccount.setCurrency(currency);
            transaction.setAccount(crossAccount);
            transaction.setDept((BigDecimal) raw[4]);
            transaction.setCredit((BigDecimal) raw[3]);
            Timestamp timestamp = (Timestamp) raw[2];
            transaction.setValueDate(timestamp.toLocalDateTime().toLocalDate());
            transaction.setStatus((String) raw[5]);
            transactionList.add(transaction);
        }
        RemittanceDc.getMutableItems().clear();
        RemittanceDc.getMutableItems().addAll(transactionList);
        return remitanceList;
    }
    public List<Transaction> loadTransactionValidated(){
        List<Transaction> transactionList = new ArrayList<>();

        List remitanceList = transactionService.getRemmitanceListValidated();
        for (Object o : remitanceList) {
            Object[] raw = ((Object[]) o);
            Transaction transaction = dataManager.create(Transaction.class);
            Currency currency = dataManager.create(Currency.class);
            currency.setName((String) raw[1]);
            CrossAccount crossAccount = dataManager.create(CrossAccount.class);
            crossAccount.setCurrency(currency);
            transaction.setAccount(crossAccount);
            transaction.setDept((BigDecimal) raw[4]);
            transaction.setCredit((BigDecimal) raw[3]);
            Timestamp timestamp = (Timestamp) raw[2];
            transaction.setValueDate(timestamp.toLocalDateTime().toLocalDate());
            transaction.setStatus((String) raw[5]);
            transactionList.add(transaction);
        }
        RemittanceDc1.getMutableItems().clear();
        RemittanceDc1.getMutableItems().addAll(transactionList);
        return remitanceList;
    }

    @Subscribe("Validatebtn")
    public void onSUBMITTEDbtnClick(Button.ClickEvent event) {
        if (remittanceTable.getSelected().size() > 0) {
            Set<Transaction> transactions = remittanceTable.getSelected();
            for (Transaction transaction : transactions) {
                transaction.setStatus(CheckerStatus.VALIDATED.name());
                remittanceTable.setSelected(transaction);
                RemittanceDc.getItem(transaction).setStatus(CheckerStatus.VALIDATED.name());
                transactionService.updateRemittanceStatus();

            }
            RemittanceDc.getMutableItems().clear();
            RemittanceDc.getMutableItems().addAll(loadTransaction());
            loadTransactionValidated();
        } else {
            message();
        }
    }

    @Subscribe("Deletebtn")
    public void onDeletebtnClick(Button.ClickEvent event) {
        if (remittanceTable.getSelected().size() > 0) {
            Set<Transaction> transactions = remittanceTable.getSelected();
            for (Transaction transaction : transactions) {
                transaction.setStatus(CheckerStatus.VALIDATED.name());
                remittanceTable.setSelected(transaction);
                RemittanceDc.getItem(transaction).setStatus(CheckerStatus.VALIDATED.name());
                transactionService.deleteRemittanceStatus();
            }
            RemittanceDc.getMutableItems().clear();
            RemittanceDc.getMutableItems().addAll(loadTransaction());
            loadTransactionValidated();
        } else {
            message();
        }
    }


    public void message() {
        notifications.create().withCaption("You should select one row at least").show();
    }

}