package com.bdc.treasury.web.screens.transactionstransfered;

import com.bdc.treasury.entity.CheckerStatus;
import com.bdc.treasury.entity.Status;
import com.bdc.treasury.entity.Transaction;
import com.bdc.treasury.entity.TransactionsTransfered;
import com.bdc.treasury.service.TransactionService;
import com.bdc.treasury.service.UserService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.gui.actions.list.CreateAction;
import com.haulmont.cuba.gui.actions.list.EditAction;
import com.haulmont.cuba.gui.actions.list.RefreshAction;
import com.haulmont.cuba.gui.actions.list.RemoveAction;
import com.haulmont.cuba.gui.components.Action;
import com.haulmont.cuba.gui.components.GroupTable;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@UiController("treasury_TransactionsTransfered.browse")
@UiDescriptor("transactions-transfered-browse.xml")
@LookupComponent("transactionsTransferedsTable")
@LoadDataBeforeShow
public class TransactionsTransferedBrowse extends StandardLookup<TransactionsTransfered> {
    @Inject
    private GroupTable<TransactionsTransfered> transactionsTransferedsTable;
    @Inject
    private DataManager dataManager;
    @Inject
    TransactionService transactionService;
    @Named("transactionsTransferedsTable.remove")
    private RemoveAction customersTableRemove;
    @Inject
    private CollectionLoader<TransactionsTransfered> transactionsTransferedsDl;
    private final static String CONTROL_maker_RULE = "control-maker";
    private final static String CONTROL_checker_RULE = "control-checker";
    @Inject
    private UserService userService;
    LoadContext loadContext1;

    @Named("transactionsTransferedsTable.edit")
    private EditAction<Transaction> transactionsTableEdit;

    @Named("transactionsTransferedsTable.create")
    private CreateAction<Transaction> transactionsTableCreate;

    @Named("transactionsTransferedsTable.refresh")
    private RefreshAction transactionsTableRefresh;

    @Subscribe("transactionsTransferedsTable.remove")
    public void onTransactionsTransferedsTableRemove(Action.ActionPerformedEvent event) {
        TransactionsTransfered transactionsTransfered = transactionsTransferedsTable.getSingleSelected();
        if (transactionsTransfered.getStatus() != null && !transactionsTransfered.getStatus().equals(CheckerStatus.VALIDATED.name())) {
            List<Transaction> transactionList = dataManager.load(Transaction.class)
                    .query("select e from treasury_Transaction e where e.transactionsTransfered =:transactionsTransfered")
                    .parameter("transactionsTransfered", transactionsTransfered)
                    .view("transaction-view")
                    .list();
            for (Transaction transaction : transactionList) {
                transactionService.deleteTransaction(transaction);
            }
        }
        customersTableRemove.execute();

    }

    @Subscribe
    public void onInit(InitEvent event) {
        customersTableRemove.setConfirmation(true);
        customersTableRemove.setConfirmationTitle("Removing customer...");
        customersTableRemove.setConfirmationMessage("Do you really want to remove the customer?");
        transactionsTransferedsDl.setMaxResults(10);
    }

    public void refreshDataContainers() {
        transactionsTableEdit.setAfterCloseHandler(transaction -> {
            transactionsTableRefresh.execute();
        });
        transactionsTableCreate.setAfterCloseHandler(transaction -> {
            transactionsTableRefresh.execute();
        });
    }

    @Install(to = "transactionsTransferedsDl", target = Target.DATA_LOADER)
    private List<TransactionsTransfered> transactionsTransferedsDlLoadDelegate(LoadContext<TransactionsTransfered> loadContext) {
        loadContext1 = loadContext;
        return loadPendingData(loadContext1);
    }

    public List<TransactionsTransfered> loadPendingData(LoadContext loadContext) {
        List<String> rolesNames = userService.getUserRoles();
        List<TransactionsTransfered> transactionList = new ArrayList<>();
        if (rolesNames.contains(CONTROL_maker_RULE)) {
            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select t from treasury_TransactionsTransfered t where (t.status = :stat1 or t.status = :stat2 or t.status = :stat3)")
                    .setParameter("stat1", Status.SUBMITTED.name())
                    .setParameter("stat2", CheckerStatus.RETURNED.name())
                    .setParameter("stat3", Status.SAVED.name());
            transactionList = dataManager.loadList(loadContext);
        }
        if (rolesNames.contains(CONTROL_checker_RULE)) {
            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select t from treasury_TransactionsTransfered t where (t.status = :stat1 ) or t.status=:stat3")
                    .setParameter("stat1", Status.SUBMITTED.name())
                    .setParameter("stat3", CheckerStatus.VALIDATED.name());
            transactionList = dataManager.loadList(loadContext);
        }
        transactionsTransferedsDl.setMaxResults(10);
        return transactionList;
    }
}