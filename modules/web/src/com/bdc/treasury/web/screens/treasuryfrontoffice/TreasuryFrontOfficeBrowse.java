package com.bdc.treasury.web.screens.treasuryfrontoffice;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.service.*;

import com.bdc.treasury.web.screens.FrontOfficeDetails;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.MetadataTools;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.components.actions.BaseAction;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.gui.screen.LookupComponent;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@UiController("treasury_TreasuryFrontOffice.browse")
@UiDescriptor("treasury-front-office-browse.xml")
@LookupComponent("treasuryFrontOfficesTable")
@LoadDataBeforeShow
public class TreasuryFrontOfficeBrowse extends StandardLookup<TreasuryFrontOffice> {

    @Inject
    private DataManager dataManager;
    @Inject
    private DateField<LocalDate> startDate;
    @Inject
    private DateField<LocalDate> endDate;
    @Inject
    private CollectionContainer<Transaction> transactionsDc;
    @Inject
    private LookupField<CrossAccount> account;
    @Inject
    private Notifications notifications;
    @Inject
    private CollectionContainer<TreasuryFrontOffice> treasuryFrontOfficesDc;
    @Inject
    private GroupTable<TreasuryFrontOffice> treasuryFrontOfficeTable;
    @Inject
    TreasuryFrontOfficeService treasuryFrontOfficeService;

    @Inject
    RemTransactionService remTransactionService;
    @Inject
    DealsService dealsService;
    @Inject
    SCHEDULAR_TRACKING_Service schedular_tracking_service;

    public List<TreasuryFrontOffice> finalTreasuryFrontOfficeList = new ArrayList<>();


    @Inject
    private CollectionContainer<CrossAccount> crossAccountsDc;
    @Inject
    private MetadataTools metadataTools;
    @Inject
    TransactionService transactionService;

    @Inject
    private Screens screens;

    @Inject
    private Label<String> TimeRemField;

    @Subscribe
    public void onInit(InitEvent event) {
        treasuryFrontOfficeTable.setItemClickAction(new BaseAction("itemClickAction")
                .withHandler(actionPerformedEvent -> {
                    FrontOfficeDetails screen = screens.create(FrontOfficeDetails.class, OpenMode.NEW_TAB);
                    screen.setCrossAccount(account.getValue());
                    if (treasuryFrontOfficeTable.getSingleSelected() != null) {
                        screen.setValueDate(treasuryFrontOfficeTable.getSingleSelected().getValueDate());
                        screens.show(screen);
                    }

                }));
        treasuryFrontOfficeTable.setEnterPressAction(new BaseAction("enterPressAction")
                .withHandler(actionPerformedEvent -> {
                    TreasuryFrontOffice treasuryFrontOffice = treasuryFrontOfficeTable.getSingleSelected();
                    if (treasuryFrontOffice != null) {
//                        popupView.setPopupVisible(true);
                        notifications.create()
                                .withCaption("Enter pressed for: " + metadataTools.getInstanceName(treasuryFrontOffice))
                                .show();
                    }
                }));
//        popupView.setPopupVisible(true);
//        popupView.setHeightAuto();
//        popupView.setWidthAuto();
        List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
                .query("select t from treasury_CrossAccount t where t.status=:status")
                .parameter("status",CheckerStatus.VALIDATED.name())
                .view("crossAccount-view")
                .list();
        crossAccountsDc.getMutableItems().clear();
        crossAccountsDc.getMutableItems().addAll(crossAccountList);
        account.setOptionsList(crossAccountList);
    }

    @Subscribe("treasuryFrontOfficeTable")
    public void onTreasuryFrontOfficeTableSelection(Table.SelectionEvent<TreasuryFrontOffice> event) {
//        FrontOfficeDetails screen = screens.create(FrontOfficeDetails.class, OpenMode.NEW_TAB);
//        screen.setCrossAccount(account.getValue());
//        if (treasuryFrontOfficeTable.getSingleSelected() != null) {
//            screen.setValueDate(treasuryFrontOfficeTable.getSingleSelected().getValueDate());
//            screens.show(screen);
//        }
    }

    @Subscribe("treasuryFrontOfficeTable.search")
    public void onTreasuryFrontOfficeTableSearch(Action.ActionPerformedEvent event) {
        if (account.getValue() != null && startDate.getValue() != null && endDate.getValue() != null) {
            treasuryFrontOfficesDc.getMutableItems().clear();
            treasuryFrontOfficesDc.getMutableItems().addAll(loadTransactionList());
            finalTreasuryFrontOfficeList = loadTransactionList();

        } else {
            notifications.create().withCaption("Must select account and start date and end date").show();
        }
        LocalDateTime date1 = null;
        LocalDateTime date2 = null;
        List rem_SCHEDULAR_List = schedular_tracking_service.get_SCHEDULAR_TRACKING("REMITTANCE");
        if (rem_SCHEDULAR_List.size() > 0) {
            for (Object o : rem_SCHEDULAR_List) {
                Object[] raw = ((Object[]) o);
                Timestamp timestamp = (Timestamp) raw[2];
                date1 = timestamp.toLocalDateTime();

            }
        }
        List deals_SCHEDULAR_List = schedular_tracking_service.get_SCHEDULAR_TRACKING("KONDOR");
        if (deals_SCHEDULAR_List.size() > 0) {
            for (Object o : deals_SCHEDULAR_List) {
                Object[] raw = ((Object[]) o);
                Timestamp timestamp = (Timestamp) raw[2];
                date2 = timestamp.toLocalDateTime();
            }
        }

        TimeRemField.setValue("Remmitance Last update time = ".concat((date1 != null ? date1.toString() : "")) + " / Deals Last update time = " + (date2 != null ? date2.toString() : ""));
    }

    public List<Transaction> getTransactionWithoutValueDate() {
        List<Transaction> transactionList = new ArrayList<>();
        return transactionList = dataManager.load(Transaction.class)
                .query("select e from treasury_Transaction e where e.account =:account and  e.valueDate is null and e.deleteTs is null ")
                .parameter("account", account.getValue())
                .view("transaction-view")
                .list();
    }

    public List<TreasuryFrontOffice> loadTransactionList() {
        boolean accountIsMain = false;
        if (account.getValue().getName() != null && account.getValue().getIsMainAccount() != null && account.getValue().getIsMainAccount()) {
            accountIsMain = true;
        }
        List transactionList = transactionService.getTransactionList(account.getValue(), startDate.getValue(), endDate.getValue(), accountIsMain);
        BigDecimal totalDebit = BigDecimal.ZERO;
        BigDecimal totalCredit = BigDecimal.ZERO;
//        for (Transaction transaction : getTransactionWithoutValueDate()) {
//            totalCredit = totalCredit.add((transaction.getCredit() != null ? transaction.getCredit() : BigDecimal.ZERO));
//            totalDebit = totalDebit.add((transaction.getDept() != null ? transaction.getDept() : BigDecimal.ZERO));
//        }
        List<TreasuryFrontOffice> treasuryFrontOfficeList = new ArrayList<>();
        for (Object o : transactionList) {
            Object[] raw = ((Object[]) o);
            TreasuryFrontOffice treasuryFrontOffice = dataManager.create(TreasuryFrontOffice.class);
            Timestamp timestamp = (Timestamp) raw[0];
            if (timestamp != null)
                treasuryFrontOffice.setValueDate(timestamp.toLocalDateTime().toLocalDate());
            treasuryFrontOffice.setCredit(totalCredit.add(((BigDecimal) raw[1]) != null ? (BigDecimal) raw[1] : BigDecimal.ZERO));
            treasuryFrontOffice.setDepit(totalDebit.add(((BigDecimal) raw[2]) != null ? (BigDecimal) raw[2] : BigDecimal.ZERO));
            // closing and opening ///
            BigDecimal closedBalance = BigDecimal.ZERO;
            if (timestamp != null) {
                List<LocalDate> lastValueDate = treasuryFrontOfficeService.getPreValueDateTransactionList(account.getValue(), timestamp.toLocalDateTime().toLocalDate());
                if (lastValueDate.size() > 0) {
                    BigDecimal openingBalance = treasuryFrontOfficeService.getOpeningBalance(account.getValue(), lastValueDate.get(0));
                    treasuryFrontOffice.setTotalExclude(openingBalance);
                }

                closedBalance = treasuryFrontOfficeService.getClosedBalance(account.getValue(), timestamp.toLocalDateTime().toLocalDate());
            }
            treasuryFrontOffice.setClosing(closedBalance);
            treasuryFrontOfficeList.add(treasuryFrontOffice);
        }

        return treasuryFrontOfficeList;
    }

}