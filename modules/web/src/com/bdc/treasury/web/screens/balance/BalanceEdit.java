package com.bdc.treasury.web.screens.balance;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.service.ClosedAccountService;
import com.bdc.treasury.service.OpeningBalanceValidationService;
import com.haulmont.cuba.core.entity.Entity;
import com.haulmont.cuba.core.global.CommitContext;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.Events;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.actions.list.CreateAction;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.model.DataContext;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@UiController("treasury_Balance.edit")
@UiDescriptor("balance-edit.xml")
@EditedEntityContainer("balanceDc")
@LoadDataBeforeShow
public class BalanceEdit extends StandardEditor<Balance> {


    @Inject
    private LookupField<String> makerStatus;
    @Inject
    private LookupField<String> checkerStatus;

    @Inject
    UserSession userSession;
    @Inject
    private InstanceContainer<Balance> balanceDc;
    @Inject
    private LookupField<String> depitCreditId;

    @Inject
    private CollectionContainer<CrossAccount> crossAccountsDc;

    @Inject
    private DataManager dataManager;
    @Inject
    private LookupField<Currency> currencyField;
    @Inject
    private TextField<BigDecimal> balanceField;
    @Inject
    private ClosedAccountService closedAccountService;
    @Inject
    private Notifications notifications;
    @Inject
    OpeningBalanceValidationService openingBalanceValidationService;
    public String eventState = "";
    @Inject
    private LookupField<CrossAccount> crossAccountField;

    public Balance globalbalance;
    @Inject
    private CollectionLoader<CrossAccount> crossAccountsDl;


    @Subscribe
    public void onInit(InitEvent event) {

        // setup drop down list
        List<String> checkerList = new ArrayList<>();
        List<String> makerList = new ArrayList<>();
        List<String> makerDepitCreditStateList = new ArrayList<>();
        makerDepitCreditStateList.add(DepitCreditState.CREDIT.name());
        makerDepitCreditStateList.add(DepitCreditState.DEBIT.name());
        checkerList.add(CheckerStatus.RETURNED.name());
        checkerList.add(CheckerStatus.VALIDATED.name());
        makerList.add(Status.SAVED.name());
        makerList.add(Status.SUBMITTED.name());
        checkerStatus.setOptionsList(checkerList);
        makerStatus.setOptionsList(makerList);
        depitCreditId.setOptionsList(makerDepitCreditStateList);
//        crossAccountsDc.getMutableItems().clear();
        List<CrossAccount> accountList = new ArrayList<>();
        crossAccountField.setOptionsList(accountList);

    }

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        if (balanceDc.getItem().getCrossAccount() == null
                && balanceDc.getItem().getBalance() == null) {
            eventState = "create";
        } else {
            eventState = "edit";
        }
        globalbalance = balanceDc.getItem();
    }


    @Subscribe(target = Target.DATA_CONTEXT)
    public void onPreCommit(DataContext.PreCommitEvent event) {
        LocalDate now = LocalDate.now();
        Group group = userSession.getUser().getGroup();
        Balance balance = balanceDc.getItem();
        if (balance.getCrossAccount() != null && balance.getCreationDate() != null) {
            if (closedAccountService.checkOfClosedCrossAccount(balance.getCrossAccount(), balance.getCreationDate(), CloseAccountState.CLOSED.name())) {
                event.preventCommit();
                notifications.create().withCaption("This account ( " + balance.getCrossAccount().getName() + " ) is closed with value date = " + balance.getCreationDate()).show();
            }
        }

        balance.setCreationDate(now);
        balance.setGroupID(group.getId());
        if (depitCreditId.getValue() != null && depitCreditId.getValue().matches(DepitCreditState.CREDIT.name())) {
            balanceDc.getItem().setBalance(balanceField.getValue());
        }
        if (depitCreditId.getValue() != null && depitCreditId.getValue().matches(DepitCreditState.DEBIT.name())) {
            balanceDc.getItem().setBalance(BigDecimal.ZERO.subtract(balanceField.getValue()));
        }
//        if (eventState.equals("edit")) {
//            balanceDc.getItem().setBalance((balanceField.getValue().abs()));
//            if (balance != null)
//                openingBalanceValidationService.updateBalance(balance);
//        }
        boolean openingBalanceExist = openingBalanceValidationService.validateOpenBalanceExist(crossAccountField.getValue(), now);
        if (openingBalanceExist && eventState.equals("create")) {
            event.preventCommit();
            notifications.create().withCaption("Opening Balance For This account ( " + balance.getCrossAccount().getName() + "  ) already is created with value date = " + balance.getCreationDate()).show();
        }
    }

    @Subscribe
    public void onAfterClose(AfterCloseEvent event) {
        if (eventState.equals("edit") && globalbalance != null) {
            if (globalbalance.getDepitCreditState().equals(DepitCreditState.CREDIT.name()))
                globalbalance.setBalance((balanceField.getValue().abs()));
            if (globalbalance.getDepitCreditState().equals(DepitCreditState.DEBIT.name()))
                globalbalance.setBalance(BigDecimal.ZERO.subtract(balanceField.getValue().abs()));
            openingBalanceValidationService.updateBalance(globalbalance);
        }
    }


    @Subscribe("currencyField")
    public void onCurrencyFieldValueChange(HasValue.ValueChangeEvent<Currency> event) {
        if (currencyField.getValue() != null && currencyField.getValue().getName() != null) {
            List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
                    .query("select t from treasury_CrossAccount t where t.currency =:currency and t.status=:status")
                    .parameter("status",CheckerStatus.VALIDATED.name())
                    .parameter("currency", currencyField.getValue())
                    .view("crossAccount-view")
                    .list();
            crossAccountsDc.getMutableItems().clear();
            crossAccountsDc.getMutableItems().addAll(crossAccountList);
            crossAccountField.setOptionsList(crossAccountList);
        } else {
            List<CrossAccount> crossAccountList = new ArrayList<>();
            crossAccountField.setOptionsList(crossAccountList);
        }
//        else {
//            List<CrossAccount> crossAccountList = dataManager.load(CrossAccount.class)
//                    .query("select t from treasury_CrossAccount t ")
//                    .view("crossAccount-view")
//                    .list();
//            crossAccountsDc.getMutableItems().clear();
//            crossAccountsDc.getMutableItems().addAll(crossAccountList);
//            crossAccountField.setOptionsList(crossAccountList);
//        }
    }


}