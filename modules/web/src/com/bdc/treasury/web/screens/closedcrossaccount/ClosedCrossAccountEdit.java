package com.bdc.treasury.web.screens.closedcrossaccount;

import com.bdc.treasury.entity.CrossAccount;
import com.haulmont.cuba.gui.model.CollectionPropertyContainer;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.treasury.entity.ClosedCrossAccount;

import javax.inject.Inject;
import java.time.LocalDate;

@UiController("treasury_ClosedCrossAccount.edit")
@UiDescriptor("closed-cross-account-edit.xml")
@EditedEntityContainer("closedCrossAccountDc")
@LoadDataBeforeShow
public class ClosedCrossAccountEdit extends StandardEditor<ClosedCrossAccount> {
    @Inject
    private InstanceContainer<ClosedCrossAccount> closedCrossAccountDc;

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        LocalDate now = LocalDate.now();
        closedCrossAccountDc.getItem().setValueDate(now);
    }
}