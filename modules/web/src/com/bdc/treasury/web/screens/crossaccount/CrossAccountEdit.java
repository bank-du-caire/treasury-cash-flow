package com.bdc.treasury.web.screens.crossaccount;

import com.bdc.treasury.entity.CheckerStatus;
import com.bdc.treasury.entity.Currency;
import com.bdc.treasury.entity.Status;
import com.bdc.treasury.service.CrossAccountService;
import com.bdc.treasury.service.UserService;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.CheckBox;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.components.LookupPickerField;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.treasury.entity.CrossAccount;

import javax.inject.Inject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@UiController("treasury_CrossAccount.edit")
@UiDescriptor("cross-account-edit.xml")
@EditedEntityContainer("crossAccountDc")
@LoadDataBeforeShow
public class CrossAccountEdit extends StandardEditor<CrossAccount> {
    @Inject
    private InstanceContainer<CrossAccount> crossAccountDc;
    @Inject
    private CheckBox isMainAccountField;
    @Inject
    CrossAccountService crossAccountService;
    @Inject
    private LookupPickerField<Currency> currencyField;
    @Inject
    private Notifications notifications;

    public String eventState = " ";

    private final static String ReconciliationRoleMaker = "Reconciliation-Role-Maker";
    private final static String ReconciliationRoleChecker = "Reconciliation-Role-Checker";
    @Inject
    private UserService userService;
    @Inject
    private LookupField<String> status;

    @Subscribe
    public void onInit(InitEvent event) {
        List<String> ReconciliationMakerList = new ArrayList<>();
        List<String> ReconciliationCheckerList = new ArrayList<>();

        ReconciliationCheckerList.add(CheckerStatus.RETURNED.name());
        ReconciliationCheckerList.add(CheckerStatus.VALIDATED.name());

        ReconciliationMakerList.add(Status.SAVED.name());
        ReconciliationMakerList.add(Status.SUBMITTED.name());

        if (userService.getUserRoles().contains(ReconciliationRoleMaker)) {
            status.setOptionsList(ReconciliationMakerList);
        } else if (userService.getUserRoles().contains(ReconciliationRoleChecker)) {
            status.setOptionsList(ReconciliationCheckerList);
        }
    }


    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        if (crossAccountDc.getItem().getCurrency() == null
                && crossAccountDc.getItem().getName() == null) {
            eventState = "create";
        }
    }

    @Subscribe
    public void onBeforeCommitChanges(BeforeCommitChangesEvent event) throws ParseException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDateTime now = LocalDateTime.now();
        String currDate = dtf.format(now);
        Date date = new SimpleDateFormat("MM/dd/yyyy").parse(currDate);
        crossAccountDc.getItem().setCreationDate(date);
        CrossAccount crossAccount = crossAccountDc.getItem();
        if (eventState.equals("create") && isMainAccountField.isChecked() && crossAccountService.currencyExist(currencyField.getValue())) {
            notifications.create().withCaption("Another Main Account Has a same currency").show();
            event.preventCommit();
        }
    }
}